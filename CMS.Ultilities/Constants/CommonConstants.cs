﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Ultilities.Constants
{
    public class CommonConstants
    {
        public const string DefaultFooterId = "DefaultFooterId";

        public const string DefaultContactId = "default";

        public class AppRole
        {
            public const string AdminRole = "Admin";
        }
        public class UserClaims
        {
            public const string Roles = "Roles";
        }
    }
}
