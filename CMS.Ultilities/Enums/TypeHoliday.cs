﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace CMS.Ultilities.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TypeHoliday
    {
        [Display(Name = "Nghỉ lễ")] NgayNghiLe = 1,
        [Display(Name = "Nghỉ Thường")] NghiThuong = 2,
    }
}
