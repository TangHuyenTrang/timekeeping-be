﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace CMS.Ultilities.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Type
    {
        [Display(Name = "ChamCong")] ChamCong = 1,
        [Display(Name = "KeToan")] KeToan = 2,
    }

}
