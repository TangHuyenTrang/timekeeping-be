﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;


namespace CMS.Ultilities.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Status
    {
        [Display(Name = "Success")] Success = 1,
        [Display(Name = "Pending")] Pending = 2,
        [Display(Name = "Refuse")] Refuse = 3,
    }
}
