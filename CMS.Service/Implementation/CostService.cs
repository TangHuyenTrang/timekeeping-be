﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Implementation
{
    public class CostService : ICostService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private ICostRepository _costRepo;
        private IGroupCostRepository _groupCostRepo;
        private IItemsCostRepository _itemCostRepo;
        private readonly AppDbContext _dbContext;
        private ITimeKeepingRepository _timeRepo;
        private readonly UserManager<AppUser> _userManager;

        public CostService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, ITimeKeepingRepository timeRepo,
            ICostRepository costRepo, IGroupCostRepository groupCostRepo, IItemsCostRepository itemCostRepo ,UserManager<AppUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _costRepo = costRepo;
            _dbContext = dbContext;
            _itemCostRepo = itemCostRepo;
            _groupCostRepo = groupCostRepo;
            _timeRepo = timeRepo;
            _userManager = userManager;
        }
        #endregion Contructor

        #region GET
        public object GetAll(int type)
        {
            var lstGroupCost = _groupCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var lstItemCost = _itemCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var result = from c in _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Type == type)
                         join l in lstGroupCost on c.GroupCostId equals l.Id into gr
                         from grCost in gr.DefaultIfEmpty()
                         join s in lstItemCost on c.ItemsCostId equals s.Id into g
                         from itemCost in g.DefaultIfEmpty()
                         select new
                         {
                             c.Id,
                             c.Note,
                             c.Title,
                             c.Currency,
                             //c.AcceptedUserId,
                             //c.CreatedUserId,
                             ImageUrl = c.ImageUrl,
                             GroupName = grCost == null ? null :  grCost.Name,
                             ItemName = itemCost == null ? null : itemCost.Name,
                             GroupCostId = grCost == null? 0 :  grCost.Id,
                             ItemsCostId = itemCost == null ? 0 : itemCost.Id,
                             CreateDate = c.DateCreated.ToShortDateString(),
                         };
            return result.ToList();
        }
        public List<ChiPhiViewModel> GetChiPhi(DateTime startDate, DateTime endDate)
        {
            var lstGroupCost = _groupCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var lstItemCost = _itemCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var users = _userManager.Users.Select(x => new { x.Id, x.FullName }).ToList();
            var result = from c in _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && startDate <= x.DateCreated  && x.DateCreated <= endDate)
                         join l in lstGroupCost on c.GroupCostId equals l.Id into gr
                         from grCost in gr.DefaultIfEmpty()
                         join s in lstItemCost on c.ItemsCostId equals s.Id into g
                         from itemCost in g.DefaultIfEmpty()
                         select new ChiPhiViewModel
                         {
                             ImageUrl = c.ImageUrl,
                             Note = c.Note,
                             Title = c.Title,
                             Currency = c.Currency,
                             UserAccept = users.Where(x => x.Id.Equals(c.AcceptedUserId)).Select(x => x.FullName).FirstOrDefault(),
                             UserCreate = users.Where(x => x.Id.Equals(c.CreatedUserId)).Select(x => x.FullName).FirstOrDefault(),
                             GroupName = grCost == null ? null: grCost.Name,
                             ItemName = itemCost == null ? null:itemCost.Name,
                             CreateDate = c.DateCreated.ToString("dd'/'MM'/'yyyy"),
                             Type = c.Type
                         };
            return result.ToList();
        }

        public object GetByTimeKeeping(int idTime)
        {
            var lstGroupCost = _groupCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var lstItemCost = _itemCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var result = from c in _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.TimeKeepingId == idTime)
                         join l in lstGroupCost on c.GroupCostId equals l.Id into gr
                         from grCost in gr.DefaultIfEmpty()
                         join s in lstItemCost on c.ItemsCostId equals s.Id into g
                         from itemCost in g.DefaultIfEmpty()
                         select new
                         {
                             c.Id,
                             c.Note,
                             c.Title,
                             c.Currency,
                             c.AcceptedUserId,
                             c.CreatedUserId,
                             c.IsAccept,
                             ImageUrl = c.ImageUrl,
                             Status = Enum.GetName(typeof(Status), c.IsAccept),
                             GroupName = grCost == null ? null : grCost.Name,
                             ItemName = itemCost == null ? null : itemCost.Name,
                             CreateDate = c.DateCreated.ToShortDateString(),
                         };
            return result.OrderBy(x=>x.CreateDate).ToList();
        }


        public object GetByUserId(Guid id)
        {
            var lstGroupCost = _groupCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var lstItemCost = _itemCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var result = from c in _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.CreatedUserId.Equals(id))
                         join l in lstGroupCost on c.GroupCostId equals l.Id into gr
                         from grCost in gr.DefaultIfEmpty()
                         join s in lstItemCost on c.ItemsCostId equals s.Id into g
                         from itemCost in g.DefaultIfEmpty()
                         select new
                         {
                             c.Id,
                             c.Note,
                             c.Title,
                             c.Currency,
                             c.AcceptedUserId,
                             c.CreatedUserId,
                             ImageUrl = c.ImageUrl,
                             GroupName = grCost == null ? null : grCost.Name,
                             ItemName = itemCost == null ? null : itemCost.Name,
                             GroupCostId = grCost == null ? 0 : grCost.Id,
                             ItemsCostId = itemCost == null ? 0 : itemCost.Id,
                             CreateDate = c.DateCreated.ToShortDateString(),
                         };
            return result.ToList();
        }

        public CostViewModel GetById(int id)
        {
            var data = _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == id).FirstOrDefault();
            return _mapper.Map<Cost, CostViewModel>(data);
        }
        #endregion GET

        #region POST
        public async Task<Cost> Add(CostCreateViewModel cost)
        {
            cost.IsAccept = (int)Ultilities.Enums.Status.Pending;
            var data = _mapper.Map<CostCreateViewModel, Cost>(cost);
            _costRepo.Add(data);
            SaveChanges();
            return data;
        }
        #endregion POST

        #region PUT
        public async Task<Cost> Update(CostViewModel cost)
        {
            //cost.Type = (int)Ultilities.Enums.Type.ChamCong;
            var data = _mapper.Map<CostViewModel, Cost>(cost);
            _costRepo.Update(data);
            SaveChanges();
            return data;
        }

        public async Task<bool> Accept(int id, int isAccept, Guid userid)
        {
            var costs = _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == id).ToList();
            var data = costs.Select(x => { x.IsAccept = isAccept; x.AcceptedUserId = userid; return x; }).FirstOrDefault();
            _costRepo.Update(data);
            SaveChanges();
            return true;
        }
        #endregion PUT


        #region DELETE
        public void Delete(int id)
        {
            var data = _costRepo.FindAll(x => x.Id == id).FirstOrDefault();
            _costRepo.Remove(data);
            SaveChanges();

        }
        #endregion DELETE
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
