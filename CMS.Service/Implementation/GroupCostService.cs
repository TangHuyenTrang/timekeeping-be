﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Implementation
{
    public class GroupCostService : IGroupCostService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IGroupCostRepository _groupCost;
        private ICostRepository _costRepository;
        private readonly AppDbContext _dbContext;

        public GroupCostService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, IGroupCostRepository groupCost, ICostRepository costRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _groupCost = groupCost;
            _dbContext = dbContext;
            _costRepository = costRepository;
        }
        #endregion Contructor

        #region GET
        public List<GroupCostViewModel> GetAll()
        {
            var data = _groupCost.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).OrderBy(x => x.Name).ToList();
            return _mapper.Map<List<GroupCost>, List<GroupCostViewModel>>(data);
        }
        public GroupCostViewModel GetById(int id)
        {
            var data = _groupCost.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == id).FirstOrDefault();
            return _mapper.Map<GroupCost, GroupCostViewModel>(data);
        }
        #endregion GET

        #region POST
        public async Task<GroupCost> Add(GroupCostCreateViewModel groupCost)
        {
            var data = _mapper.Map<GroupCostCreateViewModel, GroupCost>(groupCost);
            _groupCost.Add(data);
            SaveChanges();
            return data;
        }
        #endregion POST

        #region PUT
        public async Task<GroupCost> Update(GroupCostViewModel groupCost)
        {
            var data = _mapper.Map<GroupCostViewModel, GroupCost>(groupCost);
            _groupCost.Update(data);
            SaveChanges();
            return data;
        }
        #endregion PUT


        #region DELETE
        public bool Delete(int id)
        {
            var cost = _costRepository.FindAll(x => x.Id == id);
            if(!cost.Any())
            {
                var data = _groupCost.FindAll(x => x.Id == id).FirstOrDefault();
                _groupCost.Remove(data);
                SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion DELETE
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
