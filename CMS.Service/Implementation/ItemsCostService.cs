﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Implementation
{
    public class ItemsCostService : IItemsCostService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IItemsCostRepository _itemCostRepo;
        private readonly AppDbContext _dbContext;
        private ICostRepository _costRepository;

        public ItemsCostService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, IItemsCostRepository itemCostRepo, ICostRepository costRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _itemCostRepo = itemCostRepo;
            _dbContext = dbContext;
            _costRepository = costRepository;
        }
        #endregion Contructor

        #region GET
        public List<ItemsCostViewModel> GetAll()
        {
            var data = _itemCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).OrderBy(x => x.Name).ToList();
            return _mapper.Map<List<ItemsCost>, List<ItemsCostViewModel>>(data);
        }

        public ItemsCostViewModel GetById(int id)
        {
            var data = _itemCostRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == id).OrderBy(x => x.Name).FirstOrDefault();
            return _mapper.Map<ItemsCost, ItemsCostViewModel>(data);
        }
        #endregion GET

        #region POST
        public async Task<ItemsCost> Add(ItemsCostCreateViewModel groupCost)
        {
            var data = _mapper.Map<ItemsCostCreateViewModel, ItemsCost>(groupCost);
            _itemCostRepo.Add(data);
            SaveChanges();
            return data;
        }
        #endregion POST

        #region PUT
        public async Task<ItemsCost> Update(ItemsCostViewModel groupCost)
        {
            var data = _mapper.Map<ItemsCostViewModel, ItemsCost>(groupCost);
            _itemCostRepo.Update(data);
            SaveChanges();
            return data;
        }
        #endregion PUT


        #region DELETE
        public bool Delete(int id)
        {
            
            var cost = _costRepository.FindAll(x => x.Id == id);
            if (!cost.Any())
            {
                var data = _itemCostRepo.FindAll(x => x.Id == id).FirstOrDefault();
                _itemCostRepo.Remove(data);
                SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion DELETE
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
