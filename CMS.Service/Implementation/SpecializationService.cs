﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Implementation
{
    public class SpecializationService : ISpecializationService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private ISpecializationRepository _ispecialRepo;
        private readonly AppDbContext _dbContext;
        private readonly UserManager<AppUser> _userManager;

        public SpecializationService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, ISpecializationRepository ispecialRepo, UserManager<AppUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ispecialRepo = ispecialRepo;
            _dbContext = dbContext;
            _userManager = userManager;
        }
        #endregion Contructor

        #region GET
        public List<SpecializationViewModel> GetAll()
        {
            var data = _ispecialRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).OrderBy(x => x.Name).ToList();
            return _mapper.Map<List<Specialization>, List<SpecializationViewModel>>(data);
        }

        public SpecializationViewModel GetById(int id)
        {
            var data = _ispecialRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == id).OrderBy(x => x.Name).FirstOrDefault();
            return _mapper.Map<Specialization, SpecializationViewModel>(data);
        }
        #endregion GET

        #region POST
        public async Task<Specialization> Add(SpecializationCreateViewModel specialization)
        {
            var data = _mapper.Map<SpecializationCreateViewModel, Specialization>(specialization);
            _ispecialRepo.Add(data);
            SaveChanges();
            return data;
        }
        #endregion POST

        #region PUT
        public async Task<Specialization> Update(SpecializationViewModel specialization)
        {
            var data = _mapper.Map<SpecializationViewModel, Specialization>(specialization);
            _ispecialRepo.Update(data);
            SaveChanges();
            return data;
        }
        #endregion PUT


        #region DELETE
        public bool Delete(int id)
        {
            List<AppUser> data = _userManager.Users.ToList();
            var user = data.Where(x => x.SpecializationId == id);// check da ton tai trong project hay chua
            if (!user.Any())
            {
                var entity = _ispecialRepo.FindAll(x => x.Id == id).FirstOrDefault();
                _ispecialRepo.Remove(entity);
                SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
            

        }
        #endregion DELETE
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
