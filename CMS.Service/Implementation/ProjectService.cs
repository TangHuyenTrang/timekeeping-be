﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Implementation
{
    public class ProjectService : IProjectService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IProjectRepository _iprojectRepo;
        private IServRepository _iserviceRepo;
        private readonly AppDbContext _dbContext;
        private ITaskRepository _taskRepo;
        private ITimeKeepingRepository _timeRepo;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, ITaskRepository taskRepo,
            IProjectRepository iprojectRepo, IServRepository iserviceRepo, ITimeKeepingRepository timeRepo)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _iprojectRepo = iprojectRepo;
            _dbContext = dbContext;
            _iserviceRepo = iserviceRepo;
            _taskRepo = taskRepo;
            _timeRepo = timeRepo;
        }
        #endregion Contructor

        #region GET
        public object GetAll()
        {
            var listProject = _iprojectRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var listService = _iserviceRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var result = from l in listProject
                         join s in listService on l.ServiceId equals s.Id into g
                         from item in g.DefaultIfEmpty()
                         select new 
                         {
                             l.Id,
                             l.Name,
                             l.Description,
                             l.Location,
                             l.Value,
                             l.ContactUser,
                             l.MaximumManday,
                             StartDate = l.StartDate.ToShortDateString(),
                             EndDate = l.EndDate.ToShortDateString(),
                             Service = item.Name
                         };
            return result.ToList();
        }

        public object GetAllByUser(Guid userId)
        {
            var projectIds = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.UserId.Equals(userId)).Select(x => x.ProjectId).ToList();
            var listProject = _iprojectRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && projectIds.Contains(x.Id));
            var listService = _iserviceRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var result = from l in listProject
                         join s in listService on l.ServiceId equals s.Id into g
                         from item in g.DefaultIfEmpty()
                         select new
                         {
                             l.Id,
                             l.Name,
                             l.Description,
                             l.Location,
                             l.Value,
                             l.ContactUser,
                             l.MaximumManday,
                             StartDate = l.StartDate.ToShortDateString(),
                             EndDate = l.EndDate.ToShortDateString(),
                             Service = item.Name
                         };
            return result.ToList();
        }
        public object GetById(int id)
        {
            try
            {
                var chamCong = _timeRepo.FindAll(x => x.ProjectId == id && x.DeleteFlag != DeleteFlg.Delete && x.Status == (int)Status.Success).Count();
                var project = _iprojectRepo.FindAll(x => x.Id == id && x.DeleteFlag != DeleteFlg.Delete);
                var listService = _iserviceRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
                var result = from l in project
                             join s in listService on l.ServiceId equals s.Id into g
                             from item in g.DefaultIfEmpty()
                             select new
                             {
                                 l.Id,
                                 l.Name,
                                 l.Description,
                                 l.Location,
                                 l.Value,
                                 l.ContactUser,
                                 l.MaximumManday,
                                 l.ServiceId,
                                 StartDate = l.StartDate.ToShortDateString(),
                                 EndDate = l.EndDate.ToShortDateString(),
                                 Service = item.Name,
                                 Kpi = TinhKPI(l.MaximumManday, chamCong),
                             };
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public double TinhKPI(int maximumManday, int chamCong)
        {
           
            if(maximumManday == 0)
            {
                return 0;
            }
            else
            {
                var result =  ( chamCong / (double)maximumManday) * 100;
                return result;

            }
        }
        #endregion GET

        #region POST
        public async Task<Project> Add(ProjectCreateViewModel project, Guid userid)
        {
            project.DateCreated = DateTime.Now;
            project.UserCreated = userid;
            var data = _mapper.Map<ProjectCreateViewModel, Project>(project);
            _iprojectRepo.Add(data);
            SaveChanges();
            return data;
        }
        #endregion POST

        #region PUT
        public async Task<Project> Update(ProjectViewModel project, Guid userid)
        {
            project.DateModified = DateTime.Now;
            project.UserModified = userid;
            var data = _mapper.Map<ProjectViewModel, Project>(project);
            _iprojectRepo.Update(data);
            SaveChanges();
            return data;
        }
        #endregion PUT


        #region DELETE
        public void Delete(int id, Guid userid)
        {
            var data = _iprojectRepo.FindAll(x => x.Id == id).FirstOrDefault();
            data.DeleteFlag = 1;
            data.UserDeleted = userid;
            _iprojectRepo.RemoveFlg(data);
            SaveChanges();

        }
        #endregion DELETE
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
