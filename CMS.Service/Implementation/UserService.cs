﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Data.Entities;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Dtos;
using CMS.Data.EF;
using CMS.Infrastructure.Interfaces;
using CMS.Data.IRepositories;
using CMS.Ultilities.Enums;
using EFSoftware.Service.Interfaces.ViewModels.Login;
using Task = System.Threading.Tasks.Task;

namespace CMS.Service.Implementation
{
    public class UserService : IUserService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly UserManager<AppUser> _userManager;
        private readonly IMapper _mapper;
        private readonly AppDbContext _dbContext;
        private IUserRoleRepository _iUserRoleRepo;
        private IRoleRepository _iRoleRepo;
        private IRolePerRepository _iRolePerRepo;
        private IPermissionRepository _ipermissonRepo;
        private ISpecializationRepository _ispecialRepo;
        public UserService(UserManager<AppUser> userManager, IMapper mapper, AppDbContext dbContext, IUnitOfWork unitOfWork,
            IUserRoleRepository iUserRoleRepo,
            IRoleRepository iRoleRepo, 
            ISpecializationRepository ispecialRepo,
            IRolePerRepository iRolePerRepo,
        IPermissionRepository ipermissonRepo)
        {
            _userManager = userManager;
            _mapper = mapper;
            _dbContext = dbContext;
            _unitOfWork = unitOfWork;
            _iUserRoleRepo = iUserRoleRepo;
            _iRoleRepo = iRoleRepo;
            _ipermissonRepo = ipermissonRepo;
            _ispecialRepo = ispecialRepo;
            _iRolePerRepo = iRolePerRepo;
        }
        #endregion Contructor

        #region GET


        public async Task<object> GetAllAsync()
        {
            try
            {
                List<AppUser> data = await _userManager.Users.ToListAsync();
                var special = await _ispecialRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).ToListAsync();
                var userrole =  _iUserRoleRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
                var lstRole = _iRoleRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
                var role = from l in lstRole
                           join u in userrole on l.Id equals u.RoleId
                           select
                           new
                           {
                               l.Name,
                               l.Id,
                               u.UserId
                           };
                var query = from d in data
                            select
                            new
                            {
                                d.Id,
                                d.UserName,
                                d.Email,
                                BirthDay = d.BirthDay.ToString("dd'/'MM'/'yyyy"),
                                DateCreated = d.DateCreated.ToString("dd'/'MM'/'yyyy"),
                                d.FullName,
                                d.Level,
                                d.NetSalary,
                                d.MandaySalary,
                                d.PhoneNumber,
                                Special = special.Where(x => x.Id == d.SpecializationId).Select(x => x.Name).FirstOrDefault(),
                                Role = role.Where(x => x.UserId.Equals(d.Id)).Select(x => x.Name).FirstOrDefault()
                          };
                var result =  query.ToList();
                return result;
                //await _userManager.Users.ProjectTo<AppUserViewModel>().ToListAsync();
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public  List<Email> GetEmail()
        {
                var data =   _userManager.Users.Select(x => new Email { UserId = x.Id, Address = x.Email }).ToList();
                return data;
        }
        public class Email
        {
            public Guid UserId { get; set; }
            public string Address { get; set; }
        }
       

        public async Task<object> GetById(Guid id)
        {
            try
            {
                List<AppUser> data = await _userManager.Users.ToListAsync();
                var special = await _ispecialRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).ToListAsync();
                var query = from d in data
                            select
                            new
                            {
                                d.Id,
                                d.UserName,
                                d.Email,
                                BirthDay = d.BirthDay.ToShortDateString(),
                                DateCreated = d.DateCreated.ToShortDateString(),
                                d.FullName,
                                d.Level,
                                d.NetSalary,
                                d.MandaySalary,
                                d.PhoneNumber,
                                Special = special.Where(x => x.Id == d.SpecializationId).Select(x => x.Name).FirstOrDefault(),
                            };
                var result = query.Where(x => x.Id.Equals(id)).FirstOrDefault();
                return result;
                //await _userManager.Users.ProjectTo<AppUserViewModel>().ToListAsync();
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        //public PagedResult<AppUserViewModel> GetAllPagingAsync(string keyword, int page, int pageSize)
        //{
        //    var query = _userManager.Users;
        //    if (!string.IsNullOrEmpty(keyword))
        //        query = query.Where(x => x.FullName.Contains(keyword)
        //        || x.UserName.Contains(keyword)
        //        || x.Email.Contains(keyword));

        //    int totalRow = query.Count();
        //    query = query.Skip((page - 1) * pageSize)
        //       .Take(pageSize);

        //    var data = query.Select(x => new AppUserViewModel()
        //    {
        //        UserName = x.UserName,
        //        Avatar = x.Avatar,
        //        // BirthDay = x.BirthDay.ToString(),
        //        Email = x.Email,
        //        FullName = x.FullName,
        //        Id = x.Id,
        //        PhoneNumber = x.PhoneNumber,
        //        //DateCreated = x.DateCreated

        //    }).ToList();
        //    var paginationSet = new PagedResult<AppUserViewModel>()
        //    {
        //        Results = data,
        //        CurrentPage = page,
        //        RowCount = totalRow,
        //        PageSize = pageSize
        //    };

        //    return paginationSet;
        //}

        public async Task<List<string>> GetListPerUser(Guid id)
        {
            List<Guid> listRoleId = _iUserRoleRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete &&  id.Equals(x.UserId))
                                                 .Select(x => x.RoleId).ToList();
            List<int> listPerId = _iRolePerRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && listRoleId.Contains(x.RoleId))
                                                .Select(x => x.PermissionId).ToList();
            List<string> listPerKey = _ipermissonRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && listPerId.IndexOf(x.Id) >= 0)
                                                     .Select(x => x.Key).ToList();
            if (listPerKey != null && listPerKey.Count > 0)
            {
                listPerKey = listPerKey.Distinct().ToList();
            }
            return listPerKey;
        }

        public async Task<AppUserViewModel> GetById(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            //var roles = await _userManager.GetRolesAsync(user);
            var userVm = Mapper.Map<AppUser, AppUserViewModel>(user);

            return userVm;
        }

        public string  GetIdByRole(Guid id)
        {
            var userRole = _iUserRoleRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).Where(x => id.Equals(x.UserId)).ToList();
           // var resultList = new List<AppRole>();
            var role = _iRoleRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).ToList();
            var data = (from r in role
                       join u in userRole on r.Id equals u.RoleId
                       select r.Name).FirstOrDefault();
            return data;
        }
        #endregion GET

        #region POST
        public async Task<IdentityResult> AddAsync(AppUserModel userVm)
        {
            var user = new AppUser();
            user.FullName = userVm.FullName;
            user.Email = userVm.Email;
            user.PhoneNumber = userVm.PhoneNumber;
            user.UserName = userVm.UserName;
            user.BirthDay = userVm.BirthDay;
            user.Avatar = userVm.Avatar;
            user.PasswordHash = userVm.PasswordHash;
            user.Level = userVm.Level;
            user.NetSalary = userVm.NetSalary;
            user.MandaySalary = userVm.MandaySalary;
            user.SpecializationId = userVm.SpecializationId;
            user.DateCreated = DateTime.Now;
            user.DateModified = DateTime.Now;
            var data = await _userManager.CreateAsync(user, userVm.PasswordHash);
            SaveChanges();
            if (data.Succeeded)
            {
                user.Id = user.Id;
                var entity = new UserRole();
                entity.UserId = user.Id;
                entity.RoleId = userVm.RoleId;
                _iUserRoleRepo.Add(entity);
                SaveChanges();
            }

            return data;
        }


        #endregion POST
        public async Task DeleteAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var userRole = _iUserRoleRepo.FindAll().Where(x => id.Equals(x.UserId.ToString().ToLower())).ToList();
            foreach (var item in userRole)
            {
                _iUserRoleRepo.Remove(item);
            }
            await _userManager.DeleteAsync(user);
            SaveChanges();
        }

       

        #region PUT
        public async Task UpdateAsync(AppUserViewModel userVm)
        {
            var userupdate = await _userManager.FindByIdAsync(userVm.Id.ToString());
            userupdate.FullName = userVm.FullName;
            userupdate.UserName = userVm.UserName;
            userupdate.BirthDay = userVm.BirthDay;
           
            userupdate.PasswordHash = userupdate.PasswordHash;
            
            userupdate.Email = userVm.Email;
            userupdate.PhoneNumber = userVm.PhoneNumber;
            userupdate.NetSalary = userVm.NetSalary;
            userupdate.MandaySalary = userVm.MandaySalary;
            userupdate.Level = userVm.Level;
            userupdate.SpecializationId = userVm.SpecializationId;
            userupdate.Avatar = userVm.Avatar;
            userupdate.DateModified = DateTime.Now; 
            await _userManager.UpdateAsync(userupdate);

            var userRole = _iUserRoleRepo.FindAll(x => userVm.Id == x.UserId).ToList();
            foreach (var item in userRole)
            {
                 item.RoleId = userVm.RoleId;
                _iUserRoleRepo.Update(item);
            }
            SaveChanges();
        }

        public void DeleteListRole(string id)
        {
            var userRole = _iUserRoleRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).Where(x => id.Equals(x.UserId)).ToList();
            foreach (var item in userRole)
            {
                _iUserRoleRepo.Remove(item);
            }
            SaveChanges();
        }
        #endregion PUT

        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
