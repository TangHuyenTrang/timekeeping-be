﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMS.Service.Implementation
{
    public class RolePermissonService : IRolePerService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IRolePerRepository _iRolePerRepo;
        private IPermissionRepository _ipermissonRepo;
        private IRoleRepository _iRoleRepo;
        private readonly AppDbContext _dbContext;

        public RolePermissonService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, IRolePerRepository iRolePerRepo, IRoleRepository iRoleRepo, IPermissionRepository ipermissonRepo)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _iRolePerRepo = iRolePerRepo;
            _iRoleRepo = iRoleRepo;
            _ipermissonRepo = ipermissonRepo;
            _dbContext = dbContext;
        }
        #endregion Contructor

        //public RoleList GetById(int id)
        //{
        //    var role = _iRoleRepo.FindById(id);

        //    var rolePermisson = _iRolePerRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).Where(x => x.RoleId == id).ToList();
        //    var resultList = new List<Permisson>();
        //    var permisson = _ipermissonRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).ToList();
        //    foreach (var item in rolePermisson)
        //    {
        //        resultList.Add(permisson.Where(x => x.Id == item.PermissionId).SingleOrDefault());
        //    }


        //    return new RoleList(role.Id, role.Name, resultList);
        //}


        public void Add(RolePermissonCreateViewModel Vm)
        {

            var role = new AppRole();
            role.Name = Vm.RoleName;
            _iRoleRepo.Add(role);
            SaveChanges();

            var permissonList = Vm.PermissonList.ToList();
            foreach (var item in permissonList)
            {
                var entity = new RolePermisson();
                entity.RoleId = role.Id;
                entity.PermissionId = item.PermissonId;
                _iRolePerRepo.Add(entity);
                SaveChanges();
            }
        }

        public void Update(RolePermissonViewModel Vm)
        {
            var role = new AppRole();
            role.Name = Vm.RoleName;
            role.Id = Vm.Id;
            _iRoleRepo.Update(role);
            SaveChanges();

           // DeleteListRole(role.Id);

            var permissonList = Vm.PermissonList.ToList();
            foreach (var item in permissonList)
            {
                var entity = new RolePermisson();
                entity.RoleId = role.Id;
                entity.PermissionId = item.PermissonId;
                _iRolePerRepo.Add(entity);
            }
            SaveChanges();
        }

        //public void DeleteListRole(int id)
        //{
        //    var rolePermisson = _iRolePerRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).Where(x => x.RoleId == id).ToList();
        //    foreach (var item in rolePermisson)
        //    {
        //        _iRolePerRepo.Remove(item.Id);
        //    }
        //    SaveChanges();
        //}

        //public void Delete(int id)
        //{
        //    _iRoleRepo.Remove(id);
        //    var rolePermisson = _iRolePerRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).Where(x => x.RoleId == id).ToList();
          
        //    foreach( var item in rolePermisson)
        //    {
        //        _iRolePerRepo.Remove(item.Id);
        //    }

        //    SaveChanges();

        //}
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

}
