﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CMS.Web.Service.Implementation
{
    public class TimeKeepingService : ITimeKeepingService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private ITimeKeepingRepository _timeRepo;
        private readonly AppDbContext _dbContext;
        private IProjectRepository _iprojectRepo;
        private readonly UserManager<AppUser> _userManager;
        private ICostRepository _costRepo;
        private IUserService _userService;
        private IUserRoleRepository _iRoleRepo;
        private ITaskRepository _taskRepo;
        private IEmailCodeRepository _iemailCodeRepo;
        private IPublicHolidayRepository _iholidayRepo;

        public TimeKeepingService(IUserService userService, IUserRoleRepository iRoleRepo, IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, ITimeKeepingRepository timeRepo, UserManager<AppUser> userManager, 
            IProjectRepository iprojectRepo, ICostRepository costRepo, ITaskRepository taskRepo, IPublicHolidayRepository iholidayRepo, IEmailCodeRepository iemailCodeRepo)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _timeRepo = timeRepo;
            _dbContext = dbContext;
            _iprojectRepo = iprojectRepo;
            _userManager = userManager;
            _costRepo = costRepo;
            _userService = userService;
            _iRoleRepo = iRoleRepo;
            _taskRepo = taskRepo;
            _iholidayRepo = iholidayRepo;
            _iemailCodeRepo = iemailCodeRepo;
        }
        #endregion Contructor

        #region GET
        public object GetAll(Guid userId)
        {
            var listTimeKeeping = _timeRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).OrderByDescending(x => x.WorkTime).ToList();

            var role = _userService.GetIdByRole(userId);
            if(role.Equals("Nhân viên"))
            {
                listTimeKeeping = listTimeKeeping.Where(x => x.UserId.Equals(userId)).ToList();
            }
            var listProject = _iprojectRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            //var listTask = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var cost = _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);

            List<AppUser> data =  _userManager.Users.ToList();
            var result = from l in listTimeKeeping
                         join s in listProject on l.ProjectId equals s.Id
                         join d in data on l.UserId equals d.Id
                         //join t in listTask on s.Id equals t.ProjectId
                         select new
                         {
                             l.Id,
                             l.Description,
                             l.Image,
                             l.Document,
                             Status = Enum.GetName(typeof(Status), l.Status),
                             WorkTime = l.WorkTime.ToString("dd'/'MM'/'yyyy HH:mm:ss"),
                             Project = s.Name,
                             User = d.FullName,
                             l.Latitude,
                             l.Longitude,
                             l.Reason,
                             TaskId = l.TaskId??0,
                             Cost = cost.Where(x => x.TimeKeepingId == l.Id).FirstOrDefault() == null ? false : true,
                         };
            return result.Distinct();
        }


        public object FindAll(Guid userId, int? projectId, int? taskId, DateTime? dateTime)
        {
            var listTimeKeeping = _timeRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);

            var role = _userService.GetIdByRole(userId);
            if (role.Equals("Nhân viên"))
            {
                listTimeKeeping = listTimeKeeping.Where(x => x.UserId.Equals(userId));
            }
            var listProject = _iprojectRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var listTask = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            //var cost = _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);

            List<AppUser> data = _userManager.Users.ToList();
            var result = from l in listTimeKeeping
                         join s in listProject on l.ProjectId equals s.Id
                         join d in data on l.UserId equals d.Id
                         join t in listTask on s.Id equals t.ProjectId
                         select new
                         {
                             l.Id,
                             l.Description,
                             l.Image,
                             l.Document,
                             Status = Enum.GetName(typeof(Status), l.Status),
                             WorkTime = l.WorkTime.ToString("dd'/'MM'/'yyyy HH:mm:ss"),
                             DateTime = l.WorkTime.ToString("dd'/'MM'/'yyyy"),
                             Project = s.Name,
                             ProjectId = s.Id,
                             User = d.FullName,
                             l.Latitude,
                             l.Longitude,
                             TaskId = t.Id,
                             TaskName = t.Description,
                             //Cost = cost.Where(x => x.TimeKeepingId == l.Id).FirstOrDefault() == null ? false : true,
                         };
            if(projectId > 0)
            {
                result = result.Where(x => x.ProjectId == projectId);
            }
            if (taskId > 0 )
            {
                result = result.Where(x => x.TaskId == taskId);
            }
            if (dateTime != null)
            {
                var temp = dateTime.Value.ToString("dd'/'MM'/'yyyy");
                result = result.Where(x => x.DateTime == temp);
            }
            return result.ToList();
        }
        public object GetAllBySuccess()
        {
            var listTimeKeeping = _timeRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Status == (int)Status.Success).OrderByDescending(x => x.WorkTime);
            var listProject = _iprojectRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var listTask = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var cost = _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);

            List<AppUser> data = _userManager.Users.ToList();
            var result = (from l in listTimeKeeping
                         join s in listProject on l.ProjectId equals s.Id
                         join t in listTask on l.TaskId equals t.Id
                         join d in data on l.UserId equals d.Id
                         select new
                         {
                             l.Id,
                             l.Description,
                             l.Image,
                             l.Document,
                            // Status = Enum.GetName(typeof(Status), l.Status),
                             WorkTime = l.WorkTime.ToString("dd'/'MM'/'yyyy HH:mm:ss"),
                             Project = s.Name,
                             User = d.FullName,
                             l.Latitude,
                             l.Longitude,
                             TaskId = t.Id,
                             TaskName = t.Description,
                             Cost = cost.Where(x => x.TimeKeepingId == l.Id).Count(),
                         }).Where(x => x.Cost > 0).Distinct();
            return result.ToList();
        }
        public object GetById(int id, Guid userid)
        {
            var listTimeKeeping = _timeRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete &&  x.Id == id);
            var listProject = _iprojectRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var listTask = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            List<AppUser> data = _userManager.Users.ToList();

            var roles = _iRoleRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.UserId.Equals(userid)).Select(x => x.RoleId).FirstOrDefault();
            var result = from l in listTimeKeeping
                         join s in listProject on l.ProjectId equals s.Id
                         join t in listTask on s.Id equals t.ProjectId
                         join d in data on l.UserId equals d.Id
                         //join r in roles on d.Id equals r.UserId
                         select new
                         {
                             l.Id,
                             l.Description,
                             l.Image,
                             l.Document,
                             Status = Enum.GetName(typeof(Status), l.Status),
                             WorkTime = l.WorkTime.ToString("dd'/'MM'/'yyyy HH:mm:ss"),
                             Project = s.Name,
                             TaskId = t.Id,
                             TaskName = t.Description,
                             User = d.FullName,
                             RoleId = roles,
                             l.Latitude,
                             l.Longitude,
                             Reason= l.Reason
                         };
            return result.FirstOrDefault();
        }

        #endregion GET

        #region POST
        public async Task<int> Add(TimeKeepingCreateViewModel timeKeeping, Guid userId)
        {
            timeKeeping.WorkTime = DateTime.Now;
            var emailcode = _iemailCodeRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.UserId.Equals(userId) && x.DateCreated.Date == timeKeeping.WorkTime.Date).FirstOrDefault();
            if (emailcode == null)
            {
                return 4; //khong dung code 
            }
            if (timeKeeping.Code.Trim() != emailcode.Code.Trim())
            {
                return 1; //khong dung code 
            }
            if (timeKeeping.WorkTime.Hour >= 19)
            {
                return 2; //qua gio cham chong
            }
            timeKeeping.Status = (int)Status.Pending;
            timeKeeping.UserId = userId;
            var data = _mapper.Map<TimeKeepingCreateViewModel, TimeKeeping>(timeKeeping);
            _timeRepo.Add(data);
            SaveChanges();
            if (timeKeeping.ListCost != null)
            {
                timeKeeping.ListCost = timeKeeping.ListCost.Where(x => x.Note != null && x.ImageUrl != null && x.Currency != "0").Select(x =>
                { x.TimeKeepingId = data.Id; x.CreatedUserId = userId; x.Type = (int)Ultilities.Enums.Type.ChamCong; return x; }).ToList();
                   foreach(var item in timeKeeping.ListCost)
                   {
                       item.IsAccept = (int)Status.Pending;
                       var costs = _mapper.Map<CostCreateViewModel, Cost>(item);
                        _costRepo.Add(costs);
                   }

                SaveChanges();
            }
            return 3;
        }

        #endregion POST

        #region PUT
        //public async Task<TimeKeeping> Update(TimeKeepingViewModel TimeKeeping)
        //{
        //    var data = _mapper.Map<TimeKeepingViewModel, TimeKeeping>(TimeKeeping);
        //    _timeRepo.Update(data);
        //    SaveChanges();
        //    return data;
        //}
        public async Task<TimeKeeping> Update(int id, string fileUrl)
        {
            var data = _timeRepo.FindAll(x => x.Id == id).ToList().Select(x => { x.Document = fileUrl; return x; }).FirstOrDefault();
            _timeRepo.Update(data);
            SaveChanges();
            return data;
        }

        public async Task<TimeKeeping> Accept(int id, int isAccept, string reason)
        {
            var data = _timeRepo.FindAll(x => x.Id == id).ToList().Select(x => { x.Status = isAccept; x.Reason = reason; return x; }).FirstOrDefault();
            _timeRepo.Update(data);
            SaveChanges();
            return data;
        }

        public  List<LuongNhanVienViewModel> TinhLuong(DateTime startDate, DateTime endDate)
        {
            var lstNgayNghiLe = _iholidayRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Status == (int)TypeHoliday.NgayNghiLe).Select(x => x.Holiday.ToShortDateString()).ToList();

            var listTimeKeeping = _timeRepo.FindAll(x => x.DateCreated >= startDate && x.DateCreated <= endDate && x.Status == (int)Status.Success).ToList();
            List<AppUser> lstUser = _userManager.Users.ToList();
            var data = (from l in listTimeKeeping
                        join d in lstUser on l.UserId equals d.Id
                        select new
                        {
                            d.MandaySalary,
                            d.NetSalary,
                            d.UserName,
                            d.Id,
                            l.DateCreated,
                        }).GroupBy(x => x.Id).Select(cl => new LuongNhanVienViewModel
                        {
                            //UserId = cl.Key,
                            Name  =  cl.First().UserName,
                            SoNgayCong = cl.Count(),
                            MandaySalary = cl.First().MandaySalary,
                            NetSalary = cl.First().NetSalary,
                            //Salary = cl.Select(x => x.DateCreated).Count() * cl.First().MandaySalary + cl.First().NetSalary,
                            SumSalary = getCongThucTinhLuong(cl.Select(x => x.DateCreated).ToList(), lstNgayNghiLe, cl.First().NetSalary, cl.First().MandaySalary)
                        }).ToList();
            return data;
        }

        #endregion PUT

        private long  getCongThucTinhLuong(List<DateTime> lstDate, List<string> lstDateHoliday,   long netSalary, long mandaySalary)
        {
            var ngayNghiLe = lstDate.Where(x => lstDateHoliday.Contains(x.ToShortDateString()));
            lstDate = lstDate.Except(ngayNghiLe).ToList();
            var ngayNghiThuong = lstDate.Where(x => (x.DayOfWeek == DayOfWeek.Saturday || x.DayOfWeek == DayOfWeek.Sunday));
            var countNgayThuong = lstDate.Count() - ngayNghiLe.Count() - ngayNghiThuong.Count();
            if(countNgayThuong < 0 )
            {
                countNgayThuong = 0;
            }
            var result = (ngayNghiLe.Count() * 3 + ngayNghiThuong.Count() + countNgayThuong) * mandaySalary + netSalary;
            return result;
        }

        #region DELETE

      
        public int Delete(int id)
        {
            var data = _timeRepo.FindAll(x => x.Id == id).FirstOrDefault();
            //khong duoc phep xoa
            if (data.Status != (int)Status.Pending)
            {
                return 1;
            }
            var costs = _costRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.TimeKeepingId == id).ToList();
            _costRepo.RemoveMultiple(costs);
            _timeRepo.Remove(data);
            SaveChanges();
            return 2;
        }
        #endregion DELETE
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
