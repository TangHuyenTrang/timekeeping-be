﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMS.Service.Implementation
{
    public class RoleService : IRoleService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IRoleRepository _iRoleRepo;
        private readonly AppDbContext _dbContext;

        public RoleService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, IRoleRepository iRoleRepo)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _iRoleRepo = iRoleRepo;
            _dbContext = dbContext;
        }
        #endregion Contructor

        public List<RoleViewModel> GetAll()
        {
            var data = _iRoleRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).ToList();
            return _mapper.Map<List<AppRole>, List<RoleViewModel>>(data);
        }
        #region POST
        public AppRole Add(RoleCreateViewModel Vm)
        {
            var entity = _mapper.Map<AppRole>(Vm);
            _iRoleRepo.Add(entity);
            SaveChanges();
            return entity;
        }
        #endregion POST
        #region PUT
        public void Update(RoleViewModel Vm)
        {
            var data = _mapper.Map<AppRole>(Vm);
            _iRoleRepo.Update(data);
            SaveChanges();
        }
        #endregion PUT

        #region DELETE

        //public void Delete(int id)
        //{
        //    _iRoleRepo.Remove(id);
        //    SaveChanges();

        //}
        #endregion DELETE

        public bool CheckPermission(string functionId, string action, string[] roles)
        {
            //var functions = _functionRepository.FindAll();
            //var permissions = _permissionRepository.FindAll();
            //var query = from f in functions
            //            join p in permissions on f.Id equals p.FunctionId
            //            join r in _roleManager.Roles on p.RoleId equals r.Id
            //            where roles.Contains(r.Name) && f.Id == functionId
            //            && ((p.CanCreate && action == "Create")
            //            || (p.CanUpdate && action == "Update")
            //            || (p.CanDelete && action == "Delete")
            //            || (p.CanRead && action == "Read"))
            //            select p;
            //return query.AnyAsync();
            return true;
        }

        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}
