﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMS.Service.Implementation
{
    public class PermissonService : IPermissonService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IPermissionRepository _ipermissonRepo;
        private readonly AppDbContext _dbContext;

        public PermissonService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, IPermissionRepository ipermissonRepo)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ipermissonRepo = ipermissonRepo;
            _dbContext = dbContext;
        }
        #endregion Contructor
        public List<PermissonViewModel> GetAll()
        {
            var data = _ipermissonRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).ToList();
            return _mapper.Map<List<Permisson>, List<PermissonViewModel>>(data);
        }
    }
}
