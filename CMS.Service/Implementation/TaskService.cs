﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.EF.Repositories;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = CMS.Data.Entities.Task;


namespace CMS.Web.Service.Implementation
{
    public class TaskService : ITaskService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private ITaskRepository _taskRepo;
        private readonly AppDbContext _dbContext;
        private readonly UserManager<AppUser> _userManager;
        private IProjectRepository _iprojectRepo;
        private ITimeKeepingRepository _timeRepo;
        private IHistoryTaskRepository _historyTask;

        public TaskService(ITimeKeepingRepository timeRepo, UserManager<AppUser> userManager, IUnitOfWork unitOfWork, IMapper mapper,
            AppDbContext dbContext, ITaskRepository taskRepo, IProjectRepository iprojectRepo, IHistoryTaskRepository historyTask)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _taskRepo = taskRepo;
            _dbContext = dbContext;
            _iprojectRepo = iprojectRepo;
             _timeRepo  = timeRepo;
            _historyTask = historyTask;
        }
        #endregion Contructor

        #region GET
        public async Task<object> GetAll()
        {
            var data = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).ToList();
            var users =  _userManager.Users.Select(x => new { x.Id, x.FullName}).ToList();
            var query = from d in data
                        join u in users on d.UserId equals u.Id
                        select new
                        {
                            d.Id,
                            d.ProjectId,
                            d.Description,
                            StartDate = d.StartDate.ToShortDateString(),
                            EndDate = d.EndDate.ToShortDateString(),
                            u.FullName
                        };

            return query.ToList();
        }

        public async Task<object> GetById(int id)
        {
            var data = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == id).ToList();
            var users = _userManager.Users.Select(x => new { x.Id, x.FullName }).ToList();
            var timeKeepingId = _timeRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.TaskId == id).Select(x => x.Id).ToList();
            var query = from d in data
                        join u in users on d.UserId equals u.Id
                        select new
                        {
                            d.Id,
                            d.ProjectId,
                            d.Description,
                            StartDate = d.StartDate.ToShortDateString(),
                            EndDate = d.EndDate.ToShortDateString(),
                            u.FullName,
                            timeKeepingId = timeKeepingId
                        };

            return query.ToList();
        }
        public async Task<object> GetByIdProject(int idProject)
        {
            var data = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.ProjectId == idProject).ToList();
            var users = _userManager.Users.Select(x => new { x.Id, x.FullName }).ToList();
            var query = from d in data
                        join u in users on d.UserId equals u.Id
                        select new
                        {
                            d.Id,
                            d.ProjectId,
                            d.Description,
                            StartDate = d.StartDate.ToShortDateString(),
                            EndDate = d.EndDate.ToShortDateString(),
                            u.FullName
                        };

            return query.ToList();
        }

        public async Task<object> GetByUser(Guid userId, int projectId)
        {
            var data = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.UserId == userId && x.ProjectId == projectId).ToList();
            var users = _userManager.Users.Select(x => new { x.Id, x.FullName }).ToList();
            var query = from d in data
                        join u in users on d.UserId equals u.Id
                        select new
                        {
                            d.Id,
                            d.ProjectId,
                            d.Description,
                            StartDate = d.StartDate.ToShortDateString(),
                            EndDate = d.EndDate.ToShortDateString(),
                        };

            return query.ToList();
        }

        public async Task<object> GetProjectByUser(Guid userId)
        {
            var projectId = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.UserId == userId).Select(x => x.ProjectId).ToList();
            var projects = _iprojectRepo.FindAll(x => projectId.Contains(x.Id)).Select(x => new { x.Id, x.Name, StartDate = x.StartDate.ToShortDateString(), EndDate = x.EndDate.ToShortDateString() }).ToList();
          

            return projects.ToList();
        }

        public async Task<object> GetNoti(Guid userId, DateTime dateNow)
        {
            var tasks = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.StartDate <= dateNow && dateNow <= x.EndDate && x.UserId.Equals(userId)).ToList();
            var projects = _iprojectRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).ToList();
            var timeKeeping = _timeRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete);
            var data = from p in tasks
                       join pr in projects on p.ProjectId equals pr.Id
                       join t in timeKeeping on p.Id equals t.TaskId into g
                       from item in g.DefaultIfEmpty()
                       select new
                       { 
                           p.Description,
                           pr.Name,
                           SoNgayDuDinh = (p.EndDate - p.StartDate).TotalDays + 1,
                           SoNgayCong =g.Count()
                       };

            return data.FirstOrDefault();
        }

        public async Task<object> GetHistoryTask(int taskId)
        {
            var users = _userManager.Users.Select(x => new { x.Id, x.FullName }).ToList();
            var task = _historyTask.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.TaskId == taskId)
                .Select(x => new 
                {
                    OldUser = users.Where(y => y.Id.Equals(x.OldUser)).FirstOrDefault().FullName,
                    NewUser = users.Where(y => y.Id.Equals(x.NewUser)).FirstOrDefault().FullName,
                    DateModified = x.DateCreated.ToString("dd'/'MM'/'yyyy HH:mm:ss"),
                    UserCreated = users.Where(y => y.Id.Equals(x.UserCreated)).FirstOrDefault().FullName,
                 }).OrderBy(x => x.DateModified);
            return task.ToList();
        }

        #endregion GET

        #region POST
        public async Task<Task> Add(TaskCreateViewModel task)
        {
            var data = _mapper.Map<TaskCreateViewModel, Task>(task);
            _taskRepo.Add(data);
            SaveChanges();
            return data;
        }
        #endregion POST

        #region PUT
        public async Task<Task> Update(TaskViewModel task, Guid userid)
        {
            var oldUserId = _taskRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == task.Id).Select(x => x.UserId).FirstOrDefault();
            if (!oldUserId.Equals(task.UserId))
            {
                var newObject = new HistoryTaskCreateViewModel();
                newObject.TaskId = task.Id;
                newObject.NewUser = task.UserId;
                newObject.OldUser = oldUserId;
                newObject.UserCreated = userid;
                var history = _mapper.Map<HistoryTaskCreateViewModel, HistoryTaskUser>(newObject);
                _historyTask.Add(history);
                SaveChanges();
            }
            var data = _mapper.Map<TaskViewModel, Task>(task);
            _taskRepo.Update(data);
            SaveChanges();
            return data;
        }
        #endregion PUT


        #region DELETE
        public void Delete(int id)
        {
            var data = _taskRepo.FindAll(x => x.Id == id).FirstOrDefault();
            _taskRepo.Remove(data);
            SaveChanges();

        }
        #endregion DELETE
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
