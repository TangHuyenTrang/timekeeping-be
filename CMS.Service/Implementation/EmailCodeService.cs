﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static CMS.Service.Implementation.UserService;

namespace CMS.Web.Service.Implementation
{
    public class EmailCodeService : BackgroundService, IHostedService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEmailCodeRepository _emailCodeRepo;
        private  UserManager<AppUser> _userManager;
        private readonly AppDbContext _dbContext;
        private IUserRoleRepository _iUserRoleRepo;

        private readonly IServiceScopeFactory _scopeFactory;

        public EmailCodeService(IServiceScopeFactory scopeFactory)
        {
            //_userManager = userManager;
           // _unitOfWork = unitOfWork;
            //_mapper = mapper;
            // _userManager = new UserManager<AppUser>;
            //_emailCodeRepo = emailCodeRepo;
            //_dbContext = dbContext;
            _scopeFactory = scopeFactory;
        }
        #endregion Contructor

        protected override async System.Threading.Tasks.Task ExecuteAsync(CancellationToken stoppingToken)
        {
            System.Threading.Tasks.Task.Run(() => CheckSendMail(stoppingToken));
        }

        public async void CheckSendMail(CancellationToken stoppingToken)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
                var data = dbContext.AppUser.Select(x => new Email { UserId = x.Id, Address = x.Email }).ToList();
                var startTimeSpan = TimeSpan.Zero;
                var periodTimeSpan = TimeSpan.FromMinutes(1);

                var timer = new System.Threading.Timer(async (e) =>
                {
                    SendMail(data);
                }
                , null, startTimeSpan, periodTimeSpan);
            }
        }

        public async void SendMail(List<Email> data)
        {
            if (DateTime.Now.Hour == 14 && DateTime.Now.Minute == 15)
            {
                //from address    
                string fromaddress = "trang.tth166858@sis.hust.edu.vn";
                //to address    
                string toadresstitle = "code cham cong";

                //smtp server    
                string smtpserver = "smtp-mail.outlook.com";
                //smtp port number    
                int smtpportnumber = 587;

                var emailcodes = new List<EmailCode>();
                foreach (var item in data)
                {
                    var emailcode = new EmailCode();
                    var mimemessage = new MimeMessage();
                    mimemessage.From.Add(new MailboxAddress(toadresstitle, fromaddress));

                    mimemessage.To.Add(new MailboxAddress(toadresstitle, item.Address));

                    mimemessage.Subject = RandomString(7);
                    using (var client = new SmtpClient())
                    {
                        client.Connect(smtpserver, smtpportnumber, false);
                        client.Authenticate(fromaddress, "rylDYT94");

                        await client.SendAsync(mimemessage);
                        await client.DisconnectAsync(true);
                    }
                    emailcode.Code = mimemessage.Subject.Trim();
                    emailcode.UserId = item.UserId;
                    emailcodes.Add(emailcode);
                }

                using (var scope = _scopeFactory.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
                    dbContext.EmailCode.AddRange(emailcodes);
                    dbContext.SaveChanges();
                }

            };
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public string GetCodeEmail(int userId, DateTime date)
        {
            var code = _emailCodeRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.UserId.Equals(userId) && x.DateCreated == date).Select(x => x.Code).FirstOrDefault();
            return code;
        }

        
    }
}
