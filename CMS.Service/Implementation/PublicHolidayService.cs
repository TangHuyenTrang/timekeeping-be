﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Implementation
{
    public class PublicHolidayService : IPublicHolidayService
    {
            #region Contructor
            private IUnitOfWork _unitOfWork;
            private readonly IMapper _mapper;
            private readonly AppDbContext _dbContext;
            private readonly UserManager<AppUser> _userManager;
            private IPublicHolidayRepository _iholidayRepo;

            public PublicHolidayService(IPublicHolidayRepository iholidayRepo, UserManager<AppUser> userManager, IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext)
            {
                _userManager = userManager;
                _unitOfWork = unitOfWork;
                _mapper = mapper;
                _dbContext = dbContext;
                _iholidayRepo = iholidayRepo;
            }
            #endregion Contructor

            #region GET
            public async Task<object> GetAll()
            {
                var data = _iholidayRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete)
                .Select(x => new { x.Id, x.Description, Holiday = x.Holiday.ToString("dd'/'MM'/'yyyy"), Status = Enum.GetName(typeof(TypeHoliday), x.Status), }).ToList();
                return data;
            }

            public async Task<object> GetById(int id)
            {
                var data = _iholidayRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == id).FirstOrDefault();
                return data;
            }

            #endregion GET

            #region POST
            public async Task<PublicHoliday> Add(PublicHolidayCreateViewModel holiday)
            {
                var data = _mapper.Map<PublicHolidayCreateViewModel, PublicHoliday>(holiday);
                _iholidayRepo.Add(data);
                SaveChanges();
                return data;
            }
            #endregion POST

            #region PUT
            public async Task<PublicHoliday> Update(PublicHolidayViewModel holiday)
            {
                var data = _mapper.Map<PublicHolidayViewModel, PublicHoliday>(holiday);
                _iholidayRepo.Update(data);
                SaveChanges();
                return data;
            }
            #endregion PUT


            #region DELETE
            public void Delete(int id)
            {
                var data = _iholidayRepo.FindAll(x => x.Id == id).FirstOrDefault();
               _iholidayRepo.Remove(data);
                SaveChanges();

            }
            #endregion DELETE
            private void SaveChanges()
            {
                _unitOfWork.SaveChanges();
            }
    }
}
