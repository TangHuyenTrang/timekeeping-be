﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Ultilities.Enums;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Implementation
{
    public class ServService : IServService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IServRepository _servRepo;
        private IProjectRepository _projectRepo;
        private readonly AppDbContext _dbContext;

        public ServService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, IServRepository servRepo, IProjectRepository projectRepo)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _servRepo = servRepo;
            _dbContext = dbContext;
            _projectRepo = projectRepo;
        }
        #endregion Contructor

        #region GET
        public List<ServViewModel> GetAll()
        {
            var data = _servRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete).OrderBy(x => x.Name).ToList();
            return _mapper.Map<List<CMS.Data.Entities.Service>, List<ServViewModel>>(data);
        }

        public ServViewModel GetById(int id)
        {
            var data = _servRepo.FindAll(x => x.DeleteFlag != DeleteFlg.Delete && x.Id == id).OrderBy(x => x.Name).FirstOrDefault();
            return _mapper.Map<CMS.Data.Entities.Service, ServViewModel>(data);
        }
        #endregion GET

        #region POST
        public async Task<CMS.Data.Entities.Service> Add(ServCreateViewModel serv)
        {
            var data = _mapper.Map<ServCreateViewModel, CMS.Data.Entities.Service>(serv);
            _servRepo.Add(data);
            SaveChanges();
            return data;
        }
        #endregion POST

        #region PUT
        public async Task<CMS.Data.Entities.Service> Update(ServViewModel serv)
        {
            var data = _mapper.Map<ServViewModel, CMS.Data.Entities.Service>(serv);
            _servRepo.Update(data);
            SaveChanges();
            return data;
        }
        #endregion PUT


        #region DELETE
        public bool Delete(int id)
        {
            var project = _projectRepo.FindAll(x => x.ServiceId == id && x.DeleteFlag != 1);// check da ton tai trong project hay chua
            if(!project.Any())
            {
                var data = _servRepo.FindAll(x => x.Id == id ).FirstOrDefault();
                _servRepo.Remove(data);
                SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
       

        }
        #endregion DELETE
        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }
    }
}
