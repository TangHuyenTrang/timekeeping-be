﻿using AutoMapper;
using CMS.Data.EF;
using CMS.Data.Entities;
using CMS.Data.IRepositories;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;


namespace CMS.Service.Implementation
{
    public class UserRoleService : IUserRoleService
    {
        #region Contructor
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private UserManager<AppUser> _userManager;
        private IRoleRepository _iRoleRepo;
        private IUserRoleRepository _iUserRoleRepo;
        private readonly AppDbContext _dbContext;

        public UserRoleService(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext dbContext, IRolePerRepository iRolePerRepo, IRoleRepository iRoleRepo, IUserRoleRepository iUserRoleRepo)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _iUserRoleRepo = iUserRoleRepo;
            _iRoleRepo = iRoleRepo;
            _dbContext = dbContext;
        }
        #endregion Contructor
        public void Add(UserRoleCreateViewModel Vm)
        {

            var role = new UserRole();
            
        }


        private void SaveChanges()
        {
            _unitOfWork.SaveChanges();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}
