﻿using AutoMapper;
using CMS.Data.Entities;
using CMS.Service.ViewModels;
using CMS.Web.Service.ViewModels;
using System.Collections.Generic;


namespace CMS.Service.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AppUserModel, AppUser>().ReverseMap();

            CreateMap<AppUser, AppUserModel>().ReverseMap();

            CreateMap<AppUserViewModel, AppUser>().ReverseMap();

            CreateMap<AppUser, AppUserViewModel>().ReverseMap();

            CreateMap<AppRole, AppRoleViewModel>().ReverseMap();

            CreateMap<Permisson, PermissonViewModel>().ReverseMap();
            CreateMap<PermissonViewModel, Permisson>().ReverseMap();

            CreateMap<AppRole, RoleCreateViewModel>().ReverseMap();
            CreateMap<RoleCreateViewModel, AppRole>().ReverseMap();
            CreateMap<AppRole, RoleViewModel>().ReverseMap();
            CreateMap<RoleViewModel, AppRole>().ReverseMap();

            CreateMap<RolePermisson, RolePermissonCreateViewModel>().ReverseMap();
            CreateMap<RolePermissonCreateViewModel, RolePermisson>().ReverseMap();
            CreateMap<RolePermissonCreateViewModel, AppRole>().ReverseMap();
            CreateMap<AppRole, RolePermissonCreateViewModel>().ReverseMap();

            CreateMap<Specialization, SpecializationViewModel>().ReverseMap();
            CreateMap<SpecializationViewModel, Specialization>().ReverseMap();
            CreateMap<Specialization, SpecializationCreateViewModel>().ReverseMap();
            CreateMap<SpecializationCreateViewModel, Specialization>().ReverseMap();

            CreateMap<Project, ProjectViewModel>().ReverseMap();
            CreateMap<ProjectViewModel, Project>().ReverseMap();
            CreateMap<Project, ProjectCreateViewModel>().ReverseMap();
            CreateMap<ProjectCreateViewModel, Project>().ReverseMap();

            CreateMap<GroupCost, GroupCostCreateViewModel>().ReverseMap();
            CreateMap<GroupCostCreateViewModel, GroupCost>().ReverseMap();
            CreateMap<GroupCost, GroupCostViewModel>().ReverseMap();
            CreateMap<GroupCostViewModel, GroupCost>().ReverseMap();

            CreateMap<CMS.Data.Entities.Service, ServCreateViewModel>().ReverseMap();
            CreateMap<ServCreateViewModel, CMS.Data.Entities.Service>().ReverseMap();
            CreateMap<CMS.Data.Entities.Service, ServViewModel>().ReverseMap();
            CreateMap<ServViewModel, CMS.Data.Entities.Service>().ReverseMap();

            CreateMap<ItemsCost, ItemsCostViewModel>().ReverseMap();
            CreateMap<ItemsCostViewModel, ItemsCost>().ReverseMap();
            CreateMap<ItemsCost, ItemsCostCreateViewModel>().ReverseMap();
            CreateMap<ItemsCostCreateViewModel, ItemsCost>().ReverseMap();

            CreateMap<Cost, CostViewModel>().ReverseMap();
            CreateMap<CostViewModel, Cost>().ReverseMap();
            CreateMap<Cost, CostCreateViewModel>().ReverseMap();
            CreateMap<CostCreateViewModel, Cost>().ReverseMap();

            CreateMap<TimeKeeping, TimeKeepingCreateViewModel>().ReverseMap();
            CreateMap<TimeKeepingCreateViewModel, TimeKeeping>().ReverseMap();
            CreateMap<TimeKeeping, TimeKeepingViewModel>().ReverseMap();
            CreateMap<TimeKeepingViewModel, TimeKeeping>().ReverseMap();

            CreateMap<Task, TaskViewModel>().ReverseMap();
            CreateMap<TaskViewModel, Task>().ReverseMap();
            CreateMap<Task, TaskCreateViewModel>().ReverseMap();
            CreateMap<TaskCreateViewModel, Task>().ReverseMap();

            CreateMap<PublicHoliday, PublicHolidayViewModel>().ReverseMap();
            CreateMap<PublicHolidayViewModel, PublicHoliday>().ReverseMap();
            CreateMap<Task, PublicHolidayCreateViewModel>().ReverseMap();
            CreateMap<PublicHolidayCreateViewModel, PublicHoliday>().ReverseMap();

            CreateMap<HistoryTaskUser, HistoryTaskCreateViewModel>().ReverseMap();
            CreateMap<HistoryTaskCreateViewModel, HistoryTaskUser>().ReverseMap();

            CreateMap<EmailCode, EmailCreateViewModel>().ReverseMap();
            CreateMap<EmailCreateViewModel, EmailCode>().ReverseMap();
        }
    }
}

