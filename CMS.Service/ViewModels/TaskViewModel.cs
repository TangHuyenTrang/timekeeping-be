﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Guid UserId { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class TaskCreateViewModel
    {
        public string Description { get; set; }
        public Guid UserId { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class HistoryTaskCreateViewModel
    {
        public int TaskId { get; set; }
        public int Id { get; set; }
        public Guid NewUser { get; set; }
        public Guid OldUser { get; set; }
        public Guid? UserCreated { get; set; }
    }
}
