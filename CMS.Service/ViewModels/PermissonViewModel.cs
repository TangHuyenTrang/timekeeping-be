﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Service.ViewModels
{
    public class PermissonViewModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
