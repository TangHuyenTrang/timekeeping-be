﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class SpecializationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class SpecializationCreateViewModel
    {
        public string Name { get; set; }
    }
}
