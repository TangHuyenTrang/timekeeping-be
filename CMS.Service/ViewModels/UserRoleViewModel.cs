﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Service.ViewModels
{
    public class UserRoleViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int RoleId { get; set; }
    }

    public class UserRoleCreateViewModel
    {
        public string UserId { get; set; }
        public int RoleId { get; set; }
    }

}
