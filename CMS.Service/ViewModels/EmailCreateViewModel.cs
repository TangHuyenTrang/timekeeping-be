﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class EmailCreateViewModel
    {
        public Guid UserId { get; set; }
        public string Code { get; set; }
    }
}
