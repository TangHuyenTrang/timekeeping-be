﻿using CMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Service.ViewModels
{
    public class RolePermissonViewModel
    {
        public Guid Id { get; set; }
        public string RoleName { get; set; }
        public List<PermissonCreateView> PermissonList { get; set; }
    }
    public class PermissonView
    {
        public int Id { get; set; }
        public int PermissonId { get; set; }
    }
    public class RolePermissonCreateViewModel
    {
        public string RoleName { get; set; }
        public List<PermissonCreateView> PermissonList{get;set;}
    }
    public class PermissonCreateView
    {
        public int PermissonId { get; set; }
    }

    public class RoleList
    {
        public RoleList(Guid roleId, string roleName, List<Permisson> listPermisson)
        {
            RoleId = roleId;
            RoleName = roleName;
            this.listPermisson = listPermisson;
        }

        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public List<Permisson> listPermisson { get; set; }
    }

    public class RoleListViewModel
    {
        public RoleViewModel RoleList { get; set; }
        public List<PermissonViewModel> PermissonList { get; set; }
    }
    
}
