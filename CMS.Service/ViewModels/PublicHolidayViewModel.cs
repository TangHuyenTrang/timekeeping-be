﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class PublicHolidayViewModel
    {
        public int Id { get; set; }
        public DateTime Holiday { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    public class PublicHolidayCreateViewModel
    {
        public DateTime Holiday { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }
}
