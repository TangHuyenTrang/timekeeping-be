﻿using CMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class ProjectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public long Value { get; set; }
        public string ContactUser { get; set; }
        public int MaximumManday { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
        public int ServiceId { get; set; }
        public DateTime DateModified { get; set; }
        public Guid? UserModified { get; set; }
    }
    public class ProjectCreateViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public long Value { get; set; }
        public string ContactUser { get; set; }
        public int MaximumManday { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
        public int ServiceId { get; set; }
        public DateTime DateCreated { get; set; }
        public Guid? UserCreated { get; set; }
    }

    public class ProjectView
    {
        public Project project { get; set; }
        public string Service { get; set; }
    }
}
