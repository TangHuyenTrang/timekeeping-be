﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class LuongNhanVienViewModel
    {
        public string Name { get; set; }
        public int SoNgayCong { get; set; }
        public long MandaySalary { get; set; }
        public long NetSalary { get; set; }
        public long SumSalary { get; set; }
    }

    public class ChiPhiViewModel
    {
        public string Note { get; set; }
        public string Title { get; set; }
        public string Currency { get; set; }
        public string UserAccept { get; set; }
        public string UserCreate { get; set; }
        public string GroupName { get; set; }
        public string ItemName { get; set; }
        public string CreateDate { get; set; }
        public int Type { get; set; }
        public string ImageUrl { get; set; }
    }
}
