﻿using CMS.Data.Entities;
using CMS.Ultilities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CMS.Service.ViewModels
{
    public class AppUserViewModel
    {

        public Guid? Id { set; get; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PasswordHash { get; set; }
        public string UserName { get; set; }
        public DateTime BirthDay { set; get; }
        public string Avatar { get; set; }
        public string Level { get; set; }
        public long MandaySalary { get; set; }
        public long NetSalary { get; set; }
        public int SpecializationId { get; set; }
        public Guid RoleId { get; set; }
    }

    public class AppUserModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public DateTime BirthDay { set; get; }
        public string Avatar { get; set; }
        public string PasswordHash { get; set; }
        public Guid RoleId { get; set; }
        public string Gender { get; set; }
        public string Level { get; set; }
        public long MandaySalary { get; set; }
        public long NetSalary { get; set; }
        public int SpecializationId { get; set; }
    }
    public class UserByRoleViewModel
    {
        public UserByRoleViewModel(Guid? id, string userName, string fullname, List<AppRole> listRole)
        {
            Id = id;
            UserName = userName;
            FullName = fullname;
            this.listRole = listRole;
        }
        public Guid? Id { set; get; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public List<AppRole> listRole { get; set; }
    }
}
