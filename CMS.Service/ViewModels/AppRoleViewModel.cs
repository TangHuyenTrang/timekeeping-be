﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Service.ViewModels
{
    public class AppRoleViewModel
    {
        public Guid? Id { set; get; }

        public string Name { set; get; }
    }
}
