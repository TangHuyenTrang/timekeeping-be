﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class GroupCostViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
    public class GroupCostCreateViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
