﻿using CMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class TimeKeepingViewModel
    {
        public int Id { get; set; }
        public DateTime WorkTime { get; set; }
        public string Description { get; set; }
        public Guid UserId { get; set; }
        public string Image { get; set; }
        public string Document { get; set; }
        public int ProjectId { get; set; }
        public int Status { get; set; }
        public string Code { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int TaskId { get; set; }
    }
    public class TimeKeepingCreateViewModel
    {
        public DateTime WorkTime { get; set; }
        public string Description { get; set; }
        public Guid? UserId { get; set; }
        public string Image { get; set; }
        public string Document { get; set; }
        public int ProjectId { get; set; }
        public int TaskId { get; set; }
        public int? Status { get; set; }
        public string Code { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public List<CostCreateViewModel> ListCost { get; set; }
    }
}
