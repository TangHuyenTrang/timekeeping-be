﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class ServViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IntroDocument { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }
    public class ServCreateViewModel
    {
        public string Name { get; set; }
        public string IntroDocument { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }
}
