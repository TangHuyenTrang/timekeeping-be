﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class CostViewModel
    {
        public int Id { get; set; }
        public string Currency { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
      //  public DateTime CreateDate { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid AcceptedUserId { get; set; }
        public int? TimeKeepingId { get; set; }
        public int? GroupCostId { get; set; }
        public int? ItemsCostId { get; set; }
        public int? Type { get; set; }
        public int IsAccept { get; set; }
        public string ImageUrl { get; set; }
    }
    public class CostCreateViewModel
    {
        public string Currency { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
     //   public DateTime CreateDate { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid AcceptedUserId { get; set; }
        public int? TimeKeepingId { get; set; }
        public int? GroupCostId { get; set; }
        public int? ItemsCostId { get; set; }
        public int? Type { get; set; }
        public int IsAccept { get; set; }
        public string ImageUrl { get; set; }

    }
}
