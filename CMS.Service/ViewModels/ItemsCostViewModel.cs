﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.ViewModels
{
    public class ItemsCostViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
    }

    public class ItemsCostCreateViewModel
    {
        public string Name { get; set; }
        public string Note { get; set; }
    }

}
