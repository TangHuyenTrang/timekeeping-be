﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Interfaces
{
    public interface IServService
    {
        List<ServViewModel> GetAll();
        ServViewModel GetById(int id);
        Task<CMS.Data.Entities.Service> Add(ServCreateViewModel groupCost);
        Task<CMS.Data.Entities.Service> Update(ServViewModel groupCost);
        bool Delete(int id);
    }
}
