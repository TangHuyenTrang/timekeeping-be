﻿using CMS.Data.Entities;
using CMS.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Service.Interfaces
{
    public interface IRoleService
    {
         List<RoleViewModel> GetAll();
        AppRole Add(RoleCreateViewModel Vm);
        void Update(RoleViewModel Vm);
         //void Delete(int id);
    }
}
