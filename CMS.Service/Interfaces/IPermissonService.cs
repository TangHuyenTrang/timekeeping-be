﻿using CMS.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Service.Interfaces
{
    public interface IPermissonService
    {
        List<PermissonViewModel> GetAll();
    }
}
