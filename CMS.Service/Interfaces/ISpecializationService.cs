﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Interfaces
{
    public interface ISpecializationService
    {
        List<SpecializationViewModel> GetAll();
        SpecializationViewModel GetById(int id);
        Task<Specialization> Add(SpecializationCreateViewModel specialization);
        Task<Specialization> Update(SpecializationViewModel specialization);
        bool Delete(int id);
    }
}
