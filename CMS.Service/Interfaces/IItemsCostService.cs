﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Interfaces
{
    public interface IItemsCostService
    {
        List<ItemsCostViewModel> GetAll();
        ItemsCostViewModel GetById(int id);
        Task<ItemsCost> Add(ItemsCostCreateViewModel groupCost);
        Task<ItemsCost> Update(ItemsCostViewModel groupCost);
        bool Delete(int id);
    }
}
