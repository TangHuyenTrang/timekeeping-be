﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CMS.Service.ViewModels;
using CMS.Ultilities.Dtos;

namespace CMS.Service.Interfaces
{
    public interface IAppRoleService
    {
        Task<bool> AddAsync(AppRoleViewModel userVm);

        Task DeleteAsync(Guid id);

        Task<List<AppRoleViewModel>> GetAllAsync();

        Task<AppRoleViewModel> GetById(Guid id);

        Task UpdateAsync(AppRoleViewModel userVm);
    }
}
