﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Interfaces
{
    public interface ICostService
    {
        object GetByTimeKeeping(int idTime);
        object GetByUserId(Guid id);
        object GetAll(int type);
        CostViewModel GetById(int id);
        List<ChiPhiViewModel> GetChiPhi(DateTime startDate, DateTime endDate);
        Task<Cost> Add(CostCreateViewModel cost);
        Task<Cost> Update(CostViewModel cost);
        void Delete(int id);
        Task<bool> Accept(int id, int isAccept, Guid userid);
    }
}
