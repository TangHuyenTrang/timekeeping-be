﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Interfaces
{
    public interface IPublicHolidayService
    {
        Task<object> GetAll();
        Task<object> GetById(int id);
        Task<PublicHoliday> Add(PublicHolidayCreateViewModel holiday);
        Task<PublicHoliday> Update(PublicHolidayViewModel holiday);
        void Delete(int id);
    }
}
