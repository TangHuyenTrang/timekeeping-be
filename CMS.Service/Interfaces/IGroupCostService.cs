﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Interfaces
{
    public interface IGroupCostService
    {
        List<GroupCostViewModel> GetAll();
        GroupCostViewModel GetById(int id);
        Task<GroupCost> Add(GroupCostCreateViewModel groupCost);
        Task<GroupCost> Update(GroupCostViewModel groupCost);
        bool Delete(int id);
    }
}
