﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Interfaces
{
    public interface IProjectService
    {
        object GetAll();
        object GetById(int id);
        object GetAllByUser(Guid userId);

        Task<Project> Add(ProjectCreateViewModel project, Guid userid);
        Task<Project> Update(ProjectViewModel project, Guid userid);
        void Delete(int id, Guid userid);
    }
}
