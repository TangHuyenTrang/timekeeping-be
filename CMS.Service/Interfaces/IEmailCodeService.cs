﻿using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Web.Service.Interfaces
{
    public interface IEmailCodeService
    {
        string GetCodeEmail(int userId, DateTime date);
        Boolean Add(List<EmailCreateViewModel> cost);
    }
}
