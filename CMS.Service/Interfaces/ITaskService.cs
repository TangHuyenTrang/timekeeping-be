﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Task = CMS.Data.Entities.Task;

namespace CMS.Web.Service.Interfaces
{
    public interface ITaskService
    {
        Task<object> GetAll();
        Task<object> GetById(int id);
        Task<object> GetByIdProject(int idProject);
        Task<object> GetByUser(Guid userId, int projectId);
        Task<object> GetProjectByUser(Guid userId);
        Task<object> GetHistoryTask(int taskId);
        Task<object> GetNoti(Guid userId, DateTime dateNow);
        Task<Task> Add(TaskCreateViewModel task);
        Task<Task> Update(TaskViewModel task, Guid userid);
        void Delete(int id);
    }
}
