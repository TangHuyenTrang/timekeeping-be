﻿using CMS.Data.Entities;
using CMS.Web.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Web.Service.Interfaces
{
    public interface ITimeKeepingService
    {
        object GetAll(Guid userId);
        object GetById(int id, Guid userid);
        object FindAll(Guid userId, int? projectId, int? taskId, DateTime? dateTime);
        List<LuongNhanVienViewModel> TinhLuong(DateTime startDate, DateTime endDate);
        Task<int> Add(TimeKeepingCreateViewModel TimeKeeping, Guid userId);
        // Task<TimeKeeping> Update(TimeKeepingViewModel TimeKeeping);
        Task<TimeKeeping> Update(int id, string fileUrl);
        int Delete(int id);
        Task<TimeKeeping> Accept(int id, int isAccept, string reason);
        object GetAllBySuccess();
    }
}
