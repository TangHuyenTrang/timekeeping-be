﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CMS.Service.ViewModels;
using CMS.Ultilities.Dtos;
using EFSoftware.Service.Interfaces.ViewModels.Login;
using Microsoft.AspNetCore.Identity;
using static CMS.Service.Implementation.UserService;

namespace CMS.Service.Interfaces
{
    public interface IUserService
    {
        Task<object> GetAllAsync();
        List<Email> GetEmail();
        Task<object> GetById(Guid id);

       // PagedResult<AppUserViewModel> GetAllPagingAsync(string keyword, int page, int pageSize);

        Task<List<string>> GetListPerUser(Guid id);

        string GetIdByRole(Guid id);

        Task<IdentityResult> AddAsync(AppUserModel Vm);

        Task UpdateAsync(AppUserViewModel userVm);

        Task DeleteAsync(string id);
       // Task<bool> UpdatePassWord(string currentPass, string newPass, string UserName);
    }
}
