﻿using CMS.Data.Entities;
using CMS.Infrastructure.Interfaces;

namespace CMS.Data.IRepositories
{
    public interface ITaskRepository : IRepository<Task>
    {
    }
}
