﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("EmailCode")]
    public class EmailCode : DomainEntity<int>
    {
        public Guid UserId { get; set; }
        public string Code { get; set; }
    }
}
