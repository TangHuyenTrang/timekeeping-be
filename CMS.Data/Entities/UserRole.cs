﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("UserRole")]
    public class UserRole : DomainEntity<int>
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
    }
}
