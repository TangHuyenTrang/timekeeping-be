﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace CMS.Data.Entities
{
    [Table("ServiceProject")]
    public class ServiceProject : DomainEntity<int>
    {
        public int ServiceId { get; set; }
        public int ProjectId { get; set; }
    }
}
