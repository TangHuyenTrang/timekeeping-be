﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("GroupCost")]
    public class GroupCost : DomainEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
