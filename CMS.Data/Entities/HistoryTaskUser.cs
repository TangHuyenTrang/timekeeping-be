﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("HistoryTaskUser")]
    public class HistoryTaskUser : DomainEntity<int>
    {
        public int TaskId { get; set; }
        public Guid NewUser { get; set; }
        public Guid OldUser { get; set; }
        public Guid? UserCreated { get; set; }
        //public Guid? UserModified { get; set; }
        //public Guid? UserDeleted { get; set; }
    }
}
