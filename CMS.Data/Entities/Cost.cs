﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("Costs")]
    public class Cost : DomainEntity<int>
    {
        public string Currency { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public Guid? CreatedUserId { get; set; }
        public Guid? AcceptedUserId { get; set; }
        public int? IsAccept { get; set; }
        public int? TimeKeepingId { get; set; }
        public int? GroupCostId { get; set; }
        public int? ItemsCostId { get; set; }
        public int Type { get; set; }
        public string ImageUrl { get; set; }
    }
}
