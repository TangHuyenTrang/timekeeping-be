﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("Project")]
    public class Project : DomainEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public long Value { get; set; }
        public string ContactUser { get; set; }
        public int MaximumManday { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
        public int ServiceId { get; set; }

        public Guid? UserCreated { get; set; }
        public Guid? UserModified { get; set; }
        public Guid? UserDeleted { get; set; }
    }
}
