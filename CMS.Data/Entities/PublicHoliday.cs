﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("PublicHoliday")]
    public class PublicHoliday : DomainEntity<int>
    {
        public DateTime Holiday { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }
}
