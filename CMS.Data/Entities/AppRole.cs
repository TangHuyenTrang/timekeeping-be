﻿using CMS.Infrastructure.SharedCore;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
   
    [Table("AppRoles")]
    public class AppRole : IdentityRole<Guid>
    {
        public int DeleteFlag { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }

}
