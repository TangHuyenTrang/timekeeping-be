﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("Permisson")]
    public class Permisson : DomainEntity<int>
    { 
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
