﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("UserSpecializations")]
    public class UserSpecialization : DomainEntity<int>
    {
        public int SpecializationId { get; set; }
        public Guid UserId { get; set; }
    }
}
