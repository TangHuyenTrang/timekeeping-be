﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("ItemsCost")]
    public class ItemsCost : DomainEntity<int>
    {
        public string Name { get; set; }
        public string Note { get; set; }
    }
}
