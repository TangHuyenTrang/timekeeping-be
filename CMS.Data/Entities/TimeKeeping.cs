﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("TimeKeeping")]
    public class TimeKeeping : DomainEntity<int>
    {
        public DateTime WorkTime { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public Guid UserId { get; set; }
        public string Image { get; set; }
        public string Document { get; set; }
        public int? ProjectId { get; set; }
        public int? TaskId { get; set; }
        public int Status { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Reason { get; set; }
    }
}
