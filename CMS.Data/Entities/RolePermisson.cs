﻿using CMS.Infrastructure.SharedCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("RolePermission")]
    public class RolePermisson : DomainEntity<int>
    {
     public Guid RoleId { get; set; }
     public int PermissionId { get; set; }
    }
}
