﻿using CMS.Infrastructure.SharedCore;
using CMS.Ultilities.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CMS.Data.Entities
{
    [Table("AppUsers")]
    public class AppUser : IdentityUser<Guid>, IDomainEntity
    {
        public string FullName { get; set; }
        public DateTime BirthDay { set; get; }
        public string Avatar { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public string Level { get; set; }
        public long MandaySalary { get; set; }
        public long NetSalary { get; set; }
        public int SpecializationId { get; set; }

    }
}
