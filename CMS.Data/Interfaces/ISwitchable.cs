﻿using CMS.Ultilities.Enums;

namespace CMS.Data.Interfaces
{
    public interface ISwitchable
    {
        Status Status { set; get; }
    }
}
