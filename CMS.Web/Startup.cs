﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using CMS.Data.Entities;
using CMS.Data.EF;
using CMS.Web.Helpers;
using CMS.Infrastructure.Interfaces;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Authorization;
//using CMS.Web.Authorization;
using Microsoft.Extensions.Logging;
using CMS.Service.Interfaces;
using CMS.Service.Implementation;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using CMS.Service.AutoMapper;
using CMS.Data.IRepositories;
using CMS.Data.EF.Repositories;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using CMS.Web.Service.Implementation;
using CMS.Web.Service.Interfaces;
using CMS.Infrastructure.SharedCore;

namespace CMS.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                o => o.MigrationsAssembly("CMS.Data.EF")));

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
                
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Add AutoMapper
            //services.AddAutoMapper();

            services.AddIdentity<AppUser, AppRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            // Configure Identity
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;

                // User settings
                options.User.RequireUniqueEmail = true;
            });


            // configure jwt authentication
            var appSettingsSection = Configuration.GetSection("tokens");
            services.Configure<Tokens>(appSettingsSection);

           // configure jwt authentication
           var appSettings = appSettingsSection.Get<Tokens>();
            var key = System.Text.Encoding.ASCII.GetBytes(appSettings.Key);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            //  services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("PermissionAuthorization", policy =>
            //      policy.Requirements.Add(new PermissionAuthorizationRequirement()));
            //});


            //limit form
            services.Configure<FormOptions>(options =>
            {
               // options.ValueCountLimit = 200; // 200 items max
                options.ValueLengthLimit = 1024 * 1024 * 100; // 100MB max len form data
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllHeaders",
                      builder =>
                      {
                          builder.AllowAnyOrigin()
                                 .AllowAnyHeader()
                                 .AllowAnyMethod();
                      });
            });

            // Add application services.
            //services.AddScoped<UserManager<AppUser>, UserManager<AppUser>>();
            //services.AddScoped<RoleManager<AppRole>, RoleManager<AppRole>>();

            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<AutoMapper.IConfigurationProvider>(), sp.GetService));

            // config load default in first time
            //services.AddTransient<DbInitializer>();

            // config custom claims
           // services.AddScoped<IUserClaimsPrincipalFactory<AppUser>, CustomClaimsPrincipalFactory>();

            // config EF
            services.AddTransient(typeof(IUnitOfWork), typeof(EFUnitOfWork));
            services.AddTransient(typeof(IRepository<>), typeof(EFRepository<>));

            // config permisstion allow
            //services.AddTransient<IAuthorizationHandler, BaseAuthorizationHandler>();

            #region Config Repository

            //services.AddTransient<IPermissionRepository, PermissionRepository>();
            services.AddTransient<IPermissionRepository, PermissionRepository>();

            services.AddTransient<IRoleRepository, RoleRepository>();

            services.AddTransient<IRolePerRepository, RolePermissonRepository>();

            services.AddTransient<IUserRoleRepository, UserRoleRepository>();


            services.AddTransient<IProjectRepository, ProjectRepository>();

            services.AddTransient<ISpecializationRepository, SpecializationRepository>();

            services.AddTransient<IProjectRepository, ProjectRepository>();

            services.AddTransient<IGroupCostRepository, GroupCostRepository>();

            services.AddTransient<IItemsCostRepository, ItemsCostRepository>();

            services.AddTransient<IServRepository, ServRepository>();

            services.AddTransient<ICostRepository, CostRepository>();

            services.AddTransient<ITimeKeepingRepository, TimeKeepingRepository>();

            services.AddTransient<ITaskRepository, TaskRepository>();

            services.AddTransient<IPublicHolidayRepository, PublicHolidayRepository>();

            services.AddTransient<IHistoryTaskRepository, HistoryTaskRepository>();

            services.AddTransient<IEmailCodeRepository, EmailCodeRepository>();
            #endregion Config Repository

            #region Config Service

            services.AddTransient<IUserService, UserService>();
           
            services.AddTransient<IPermissonService, PermissonService>();

            services.AddTransient<IRoleService, RoleService>();

            services.AddTransient<IRolePerService, RolePermissonService>();

            services.AddTransient<ISpecializationService, SpecializationService>();


            services.AddTransient<IUserRoleService, UserRoleService>();

            services.AddTransient<IProjectService, ProjectService>();

            services.AddTransient<IGroupCostService, GroupCostService>();

            services.AddTransient<IServService, ServService>();

            services.AddTransient<IItemsCostService, ItemsCostService>();

            services.AddTransient<ICostService, CostService>();

            services.AddTransient<ITimeKeepingService, TimeKeepingService>();

            services.AddTransient<ITaskService, TaskService>();

            services.AddTransient<IPublicHolidayService, PublicHolidayService>();

           // services.AddTransient<IEmailCodeService, EmailCodeService>();

            #endregion Config Service

            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            //services.AddMvc();

            services.AddRouting();
            services.AddSingleton<Microsoft.Extensions.Hosting.IHostedService, EmailCodeService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // config folder log file
            loggerFactory.AddFile("Logs/cms-{Date}.txt");

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();

            app.UseStaticFiles();
            //app.UseStaticFiles(new StaticFileOptions()
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
            //    RequestPath = new PathString("/Resources")
            //});


            app.UseCookiePolicy();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
          
            app.UseCors("AllowAllHeaders");

            //app.UseCors(builder => builder
            //  .AllowAnyOrigin()
            //  .AllowAnyMethod()
            //  .AllowAnyHeader()
            //  .AllowCredentials());

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
