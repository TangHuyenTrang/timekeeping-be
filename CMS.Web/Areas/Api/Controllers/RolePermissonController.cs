﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Web.Areas.Api.Controllers
{
    public class RolePermissonController : BaseController
    {
        #region Constructor
        private IRolePerService _iRolePerService;

        public RolePermissonController(IRolePerService iRolePerService)
        {
            _iRolePerService = iRolePerService;
        }
        #endregion Constructor


        #region POST
        [HttpPost]
        [Route("Add")]
        public IActionResult Add([FromBody] RolePermissonCreateViewModel Vm)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    _iRolePerService.Add(Vm);

                    return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion POST


        #region PUT
        //[HttpPut]
        //[Route("Update")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_role)]
        //public IActionResult Update([FromBody]RolePermissonViewModel Vm)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        var allErrors = ModelState.Values.SelectMany(v => v.Errors);
        //        return new BadRequestObjectResult(new GenericResult(false, allErrors));
        //    }
        //    else
        //    {
        //        try
        //        {
        //            _iRolePerService.Update(Vm);

        //            return new OkObjectResult(new GenericResult(true, "Update Success"));
        //        }
        //        catch (Exception ex)
        //        {
        //            return new OkObjectResult(new GenericResult(false, ex.Message));
        //        }
        //    }
        //}
        #endregion PUT
        #region DELETE

        //[HttpDelete]
        //[Route("Delete")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_role)]
        //public IActionResult Delete(int id)
        //{
        //    if (id == 0)
        //    {
        //        return new BadRequestObjectResult(new GenericResult(false, "Id is Requied"));
        //    }
        //    else
        //    {
        //        try
        //        {
        //            _iRolePerService.Delete(id);

        //            return new OkObjectResult(new GenericResult(true, "DeleteSuccess"));
        //        }
        //        catch (Exception ex)
        //        {
        //            return new OkObjectResult(new GenericResult(false, ex.Message));
        //        }
        //    }
        //}



        #endregion DELETE
    }
}