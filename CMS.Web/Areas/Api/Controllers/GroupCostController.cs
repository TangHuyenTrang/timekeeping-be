﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Data.Entities;
using CMS.Ultilities.Dtos;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Web.Areas.Api.Controllers
{
    public class GroupCostController : BaseController
    {
        #region Constructor
        private IGroupCostService _groupCostService;

        public GroupCostController(IGroupCostService groupCostService)
        {
            _groupCostService = groupCostService;
        }
        #endregion Constructor

        #region GET
        [HttpGet]
        [Route("GetAll")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetAll()
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    var data = _groupCostService.GetAll();
                    return new OkObjectResult(new GenericResult(true, data));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }

        [HttpGet]
        [Route("GetById")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetById(int id)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    var data = _groupCostService.GetById(id);
                    return new OkObjectResult(new GenericResult(true, data));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion GET

        #region POST
        [HttpPost]
        [Route("Add")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Add([FromBody] GroupCostCreateViewModel project)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }

            else
            {
                try
                {
                    _groupCostService.Add(project);

                    return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion POST

        #region PUT
        [HttpPut]
        [Route("Update")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Update([FromBody] GroupCostViewModel project)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }

            else
            {
                try
                {
                    _groupCostService.Update(project);

                    return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion POST

        #region DELETE
        [HttpDelete]
        [Route("Delete")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_production)]
        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new BadRequestObjectResult(new GenericResult(false, "Id is Requied"));
            }
            else
            {
                try
                {
                    var data = _groupCostService.Delete(id);

                    return new OkObjectResult(new GenericResult(data, true, "DeleteSuccess"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
    }
    #endregion DELETE
}