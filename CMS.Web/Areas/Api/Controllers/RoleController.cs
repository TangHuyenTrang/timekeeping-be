﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CMS.Infrastructure.Interfaces;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Web.Areas.Api.Controllers
{
    public class RoleController : BaseController
    {
        #region Constructor
        private readonly IRoleService _iroleService;

        public RoleController(IRoleService iroleService)
        {
            _iroleService = iroleService;
        }
        #endregion Constructor

        [HttpGet]
        [Route("GetAll")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.view_role, PermissionRule.view_user)]
        public IActionResult GetAll()
        {
            try
            {
                var data = _iroleService.GetAll();
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        #region POST
        [HttpPost]
        [Route("Add")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_role)]
        public IActionResult Add([FromBody]RoleCreateViewModel Vm)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    _iroleService.Add(Vm);

                    return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion POST

        #region PUT
        [HttpPut]
        [Route("Update")]
        public IActionResult Update([FromBody]RoleViewModel Vm)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    _iroleService.Update(Vm);

                    return new OkObjectResult(new GenericResult(true, "Update Success"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion PUT

        #region DELETE

        //[HttpDelete]
        //[Route("Delete")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_role)]
        //public IActionResult Delete(int id)
        //{
        //    if (id == 0)
        //    {
        //        return new BadRequestObjectResult(new GenericResult(false, "Id is Requied"));
        //    }
        //    else
        //    {
        //        try
        //        {
        //            _iroleService.Delete(id);

        //            return new OkObjectResult(new GenericResult(true, "DeleteSuccess"));
        //        }
        //        catch (Exception ex)
        //        {
        //            return new OkObjectResult(new GenericResult(false, ex.Message));
        //        }
        //    }
        //}

        #endregion DELETE
    }
}
    