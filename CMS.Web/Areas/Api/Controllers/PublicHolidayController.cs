﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Ultilities.Dtos;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Web.Areas.Api.Controllers
{
    public class PublicHolidayController : BaseController
    {
        #region Constructor
        private IPublicHolidayService _holidayService;

        public PublicHolidayController(IPublicHolidayService holidayService)
        {
            _holidayService = holidayService;
        }
        #endregion Constructor

        #region GET
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
           
                try
                {
                    var data = _holidayService.GetAll();
                    return new OkObjectResult(new GenericResult(true, data));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            
        }

        [HttpGet]
        [Route("GetById")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetById(int id)
        {
            try
            {
                var data = _holidayService.GetById(id);
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        #endregion GET

        #region POST
        [HttpPost]
        [Route("Add")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Add([FromBody] PublicHolidayCreateViewModel holiday)
        {
               try { 
           
                _holidayService.Add(holiday);

                return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            
        }
        #endregion POST

        #region PUT
        [HttpPut]
        [Route("Update")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Update([FromBody] PublicHolidayViewModel holiday)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }

            else
            {
                try
                {
                    _holidayService.Update(holiday);

                    return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion POST

        #region DELETE
        [HttpDelete]
        [Route("Delete")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_production)]
        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new BadRequestObjectResult(new GenericResult(false, "Id is Requied"));
            }
            else
            {
                try
                {
                    _holidayService.Delete(id);

                    return new OkObjectResult(new GenericResult(true, "DeleteSuccess"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion DELETE
    }
}