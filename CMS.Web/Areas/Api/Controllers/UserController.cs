﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CMS.Service.Interfaces;
using CMS.Service.ViewModels;
using CMS.Ultilities.Dtos;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using EFSoftware.Service.Interfaces.ViewModels.Login;
using Microsoft.AspNetCore.Authorization;
using CMS.Data.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace CMS.Web.Areas.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        #region Constructor
        private readonly UserManager<AppUser> _userManager;
        private IUserService _userService;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IConfiguration _config;

        public UserController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IUserService userService, IConfiguration config)
        {
            _userManager = userManager;
            _userService = userService;
            _signInManager = signInManager;
            _config = config;
        }
        #endregion Constructor

        #region LOGIN
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(model);
            }
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user != null)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, true);
                if (result.IsLockedOut)
                {
                    return new ObjectResult(new GenericResult(false, "Login failed!"));
                }
                else if (!result.Succeeded)
                {
                    return new ObjectResult(new GenericResult(false, "Login failed!"));
                }
                var claims = new[]
                {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim("roles", "RoleName"),
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(_config["Tokens:Issuer"],
                    _config["Tokens:Issuer"],
                    claims,
                    expires: DateTime.UtcNow.AddDays(100),
                    signingCredentials: creds);

                var token_access = new JwtSecurityTokenHandler().WriteToken(token);

                return new ObjectResult(new GenericResult(true, token_access));
            }
            return new ObjectResult(new GenericResult(false, "Login failed!"));
        }
        #endregion LOGIN

        #region GET
        [HttpGet]
        [Route("GetAll")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.view_user)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var data = await _userService.GetAllAsync();
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetById")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.view_user)]
        public async Task<IActionResult> GetById(Guid id)
        {
            try
            {
                var data = await _userService.GetById(id);
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetIdByRole")]
        public async Task<IActionResult> GetIdByRole(Guid id)
        {
            if (id == null)
            {
                return new BadRequestObjectResult(new GenericResult(false, "input is not valid"));
            }
            else
            {
                try
                {
                    var data = _userService.GetIdByRole(id);
                    return new OkObjectResult(new GenericResult(true, data));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }

        [HttpGet]
        [Route("GetListPerUser")]
        public IActionResult GetListPerUser()
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity.Claims.Count() != 0)
                {
                    var  userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var data = _userService.GetListPerUser(Guid.Parse(userid));
                    return new OkObjectResult(new GenericResult(true, data));
                }
                return new OkObjectResult(new GenericResult(true, null));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }
        #endregion GET

        #region POST
        [HttpPost]
        [Route("Add")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_user)]
        public async Task<IActionResult> Add([FromBody] AppUserModel Vm)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    IdentityResult data = await _userService.AddAsync(Vm);
                
                    if (data.Succeeded)
                    {
                        return new OkObjectResult(new GenericResult(true));
                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(false, data.Errors.FirstOrDefault().Description));
                    }
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }

        #endregion POST

        #region PUT
        [HttpPut]
        [Route("Update")]
        // [AppAuthorize(PermissionTypes.Any, PermissionRule.update_user)]
        public async Task<IActionResult> Update([FromBody]AppUserViewModel Vm)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    await _userService.UpdateAsync(Vm);
                    return new OkObjectResult(new GenericResult(true, "Update Success"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion PUT

        #region DELETE

        [HttpDelete]
        [Route("Delete")]
       // [AppAuthorize(PermissionTypes.Any, PermissionRule.update_user)]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new BadRequestObjectResult(new GenericResult(false, "Id is Requied"));
            }
            else
            {
                try
                {
                    await _userService.DeleteAsync(id);

                    return new OkObjectResult(new GenericResult(true, "DeleteSuccess"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }

        #endregion DELETE
    }
}