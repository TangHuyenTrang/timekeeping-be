﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using CMS.Data.Entities;
using CMS.Ultilities.Dtos;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace CMS.Web.Areas.Api.Controllers
{
    public class TimeKeepingController : BaseController
    {
        #region Constructor
        private ITimeKeepingService _timeService;
        private ICostService _costService;
        private readonly IHostingEnvironment _environment;

        public TimeKeepingController(ITimeKeepingService timeService, IHostingEnvironment environment, ICostService costService)
        {
            _timeService = timeService;
            _costService = costService;
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
        }
        #endregion Constructor

        #region GET
        [HttpGet]
        [Route("GetAll")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetAll()
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    var identity = HttpContext.User.Identity as ClaimsIdentity;     
                    if (identity.Claims.Count() != 0)
                    {
                        var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                        var data = _timeService.GetAll(Guid.Parse(userid));
                        return new OkObjectResult(new GenericResult(true, data));
                    }
                    return new OkObjectResult(new GenericResult(true, "Bạn không đủ quyền"));
                }
                catch (Exception ex)
                 {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }

     

        [HttpGet]
        [Route("FindAll")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult FindAll(Guid userId, int? projectId, int? taskId, DateTime? dateTime)
        {

            try
            {
                var data = _timeService.FindAll(userId, projectId, taskId, dateTime);
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetAllBySuccess")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetAllBySuccess()
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    var data = _timeService.GetAllBySuccess();
                    return new OkObjectResult(new GenericResult(true, data));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }

        [HttpGet]
        [Route("GetById")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetById(int id)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    var identity = HttpContext.User.Identity as ClaimsIdentity;
                    if (identity.Claims.Count() != 0)
                    {
                        var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                        var data = _timeService.GetById(id, Guid.Parse(userid));

                        return new OkObjectResult(new GenericResult(true, data));
                    }
                    return new OkObjectResult(new GenericResult(true, "Bạn cần đăng nhập lại"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        [HttpGet]
        [Route("Accept")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Accept(int id, int isAccept, string reason)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }
            else
            {
                try
                {
                    var data = _timeService.Accept(id, isAccept, reason);
                    return new OkObjectResult(new GenericResult(true, data));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion GET

        #region POST
        [HttpPost]
        [Route("Add")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Add([FromBody] TimeKeepingCreateViewModel project)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }

            else
            {
                try
                {
                    var identity = HttpContext.User.Identity as ClaimsIdentity;
                    if (identity.Claims.Count() != 0)
                    {
                        var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                        var data = _timeService.Add(project, Guid.Parse(userid));
                        return new OkObjectResult(new GenericResult(true, data));
                    }
                    return new OkObjectResult(new GenericResult(true, "Bạn không đủ quyền"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }


        [HttpPost]
        [Route("UploadFile")]
        public async Task<IActionResult> UploadFile([FromForm]IFormFile images)
        {
            var files = Request.Form.Files[0];
            var folderName = Path.Combine("Resources", "Images");
            var pathToSave = Path.Combine(_environment.WebRootPath, folderName);
            if (files != null || files.Length > 0)
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + Path.GetExtension(files.FileName);
                string fullPath = Path.Combine(pathToSave, fileName);
                string dbPath = Path.Combine(folderName, fileName);
                fullPath = fullPath.Replace("\\", "/");
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    images.CopyTo(stream);
                }
                return new OkObjectResult(new GenericResult(true, dbPath));
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("UploadDocument")]
        public async Task<IActionResult> UploadDocument([FromForm]IFormFile images)
        {
            var files = Request.Form.Files[0];
            var folderName = Path.Combine("Resources", "Files");
            var pathToSave = Path.Combine(_environment.WebRootPath, folderName);
            if (files != null || files.Length > 0)
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + Path.GetExtension(files.FileName);
                string fullPath = Path.Combine(pathToSave, fileName);
                string dbPath = Path.Combine(folderName, fileName);
                fullPath = fullPath.Replace("\\", "/");
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    images.CopyTo(stream);
                }
                return new OkObjectResult(new GenericResult(true, dbPath));
            }
            else
            {
                return BadRequest();
            }
        }


        #endregion POST

        #region PUT
        //[HttpPut]
        //[Route("Update")]
        ////[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        //public IActionResult Update([FromBody] TimeKeepingViewModel project)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        var allErrors = ModelState.Values.SelectMany(v => v.Errors);
        //        return new BadRequestObjectResult(new GenericResult(false, allErrors));
        //    }

        //    else
        //    {
        //        try
        //        {
        //            _timeService.Update(project);

        //            return new OkObjectResult(new GenericResult(true, "Add success!!!"));
        //        }
        //        catch (Exception ex)
        //        {
        //            return new OkObjectResult(new GenericResult(false, ex.Message));
        //        }
        //    }
        //}
        [HttpGet]
        [Route("Update")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Update(int id, string fileUrl)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }

            else
            {
                try
                {
                    _timeService.Update(id, fileUrl);

                    return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion POST

        #region DELETE
        [HttpDelete]
        [Route("Delete")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_production)]
        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new BadRequestObjectResult(new GenericResult(false, "Id is Requied"));
            }
            else
            {
                try
                {
                    var result = _timeService.Delete(id);

                    return new OkObjectResult(new GenericResult(true, result));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion DELETE


        [HttpGet("TinhLuong")]
        public IActionResult TinhLuong(DateTime startDate, DateTime endDate)
        {
            var data = _timeService.TinhLuong(startDate, endDate);
            var downloadStream = new MemoryStream();
            var webRoot = _environment.WebRootPath;
            var fTemplate = System.IO.Path.Combine(webRoot, "TeamplateFile", "bangLuong2.xlsx");
            var fFile = System.IO.Path.Combine(webRoot, "TeamplateFile", "bangLuong.xlsx");
            System.IO.File.Copy(fTemplate, fFile, true);

            var workbook = new XSSFWorkbook(fFile);
            var boldFont = workbook.CreateFont();
            boldFont.FontHeightInPoints = 11;
            boldFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            ICellStyle styleDark = workbook.CreateCellStyle();
            styleDark.BorderBottom = BorderStyle.Thin;
            styleDark.BorderLeft = BorderStyle.Thin;
            styleDark.BorderRight = BorderStyle.Thin;
            styleDark.BorderTop = BorderStyle.Thin;
            styleDark.VerticalAlignment = VerticalAlignment.Center;
            styleDark.Alignment = HorizontalAlignment.Center;
            styleDark.WrapText = true;

            ICellStyle center = workbook.CreateCellStyle();
            center.BorderBottom = BorderStyle.Thin;
            center.BorderLeft = BorderStyle.Thin;
            center.BorderRight = BorderStyle.Thin;
            center.BorderTop = BorderStyle.Thin;
            center.VerticalAlignment = VerticalAlignment.Center;
            center.WrapText = true;


            ICellStyle left = workbook.CreateCellStyle();
            left.BorderBottom = BorderStyle.Thin;
            left.BorderLeft = BorderStyle.Thin;
            left.BorderRight = BorderStyle.Thin;
            left.BorderTop = BorderStyle.Thin;
            left.Alignment = HorizontalAlignment.Left;
            left.VerticalAlignment = VerticalAlignment.Center;
            left.WrapText = true;

            var italic = workbook.CreateFont();
            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            try
            {
                int col = 0, row = 6;
                var sheet = workbook.GetSheetAt(0);

                var i = 1;

                foreach (var item in data)
                {
                    sheet.CreateRow(row);

                    sheet.GetRow(row).CreateCell(col);        // STT
                    sheet.GetRow(row).CreateCell(col + 1);    // Họ tên
                    sheet.GetRow(row).CreateCell(col + 2);    // Số ngày công
                    sheet.GetRow(row).CreateCell(col + 3);    // Lương Manday
                    sheet.GetRow(row).CreateCell(col + 4);    // Lương cứng
                    sheet.GetRow(row).CreateCell(col + 5);    // Tổng lương

                    sheet.GetRow(row).GetCell(col).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 1).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 2).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 3).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 4).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 5).CellStyle = styleDark;


                    sheet.GetRow(row).GetCell(col).SetCellValue(i.ToString());
                    sheet.GetRow(row).GetCell(col + 1).SetCellValue(item.Name);
                    sheet.GetRow(row).GetCell(col + 2).SetCellValue(item.SoNgayCong);
                    sheet.GetRow(row).GetCell(col + 3).SetCellValue(item.MandaySalary);
                    sheet.GetRow(row).GetCell(col + 4).SetCellValue(item.NetSalary);
                    sheet.GetRow(row).GetCell(col + 5).SetCellValue(item.SumSalary);

                    row++;
                    i++;
                }

                workbook.Write(downloadStream);
                workbook.Close();
                System.IO.File.Delete(fFile);
                return File(downloadStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export_bang_luong" + "_.xlsx");
            }
            catch (Exception e)
            {
                workbook.Close();
                System.IO.File.Delete(fFile);
                return new OkObjectResult(new GenericResult(false, e.Message));
            }
        }

        [HttpGet("TinhChiPhi")]
        public IActionResult TinhChiPhi(DateTime startDate, DateTime endDate)
        {
            var data = _costService.GetChiPhi(startDate, endDate);
            var keToan = data.Where(x => x.Type == 2).ToList();
            var nhanVien = data.Where(x => x.Type == 1).ToList();
            var downloadStream = new MemoryStream();
            var webRoot = _environment.WebRootPath;
            var fTemplate = System.IO.Path.Combine(webRoot, "TeamplateFile", "Chi_Phi2.xlsx");
            var fFile = System.IO.Path.Combine(webRoot, "TeamplateFile", "Chi_Phi.xlsx");
            System.IO.File.Copy(fTemplate, fFile, true);

            var workbook = new XSSFWorkbook(fFile);
            var boldFont = workbook.CreateFont();
            boldFont.FontHeightInPoints = 11;
            boldFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            ICellStyle styleDark = workbook.CreateCellStyle();
            styleDark.BorderBottom = BorderStyle.Thin;
            styleDark.BorderLeft = BorderStyle.Thin;
            styleDark.BorderRight = BorderStyle.Thin;
            styleDark.BorderTop = BorderStyle.Thin;
            styleDark.VerticalAlignment = VerticalAlignment.Center;
            styleDark.Alignment = HorizontalAlignment.Center;
            styleDark.WrapText = true;

            ICellStyle center = workbook.CreateCellStyle();
            center.BorderBottom = BorderStyle.Thin;
            center.BorderLeft = BorderStyle.Thin;
            center.BorderRight = BorderStyle.Thin;
            center.BorderTop = BorderStyle.Thin;
            center.VerticalAlignment = VerticalAlignment.Center;
            center.WrapText = true;


            ICellStyle left = workbook.CreateCellStyle();
            left.BorderBottom = BorderStyle.Thin;
            left.BorderLeft = BorderStyle.Thin;
            left.BorderRight = BorderStyle.Thin;
            left.BorderTop = BorderStyle.Thin;
            left.Alignment = HorizontalAlignment.Left;
            left.VerticalAlignment = VerticalAlignment.Center;
            left.WrapText = true;

            var italic = workbook.CreateFont();
            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            try
            {
                int col = 0, row = 6;
                var sheet = workbook.GetSheetAt(0);

                var i = 1;

                foreach (var item in keToan)
                {
                    sheet.CreateRow(row);

                    sheet.GetRow(row).CreateCell(col);        // STT
                    sheet.GetRow(row).CreateCell(col + 1);    // Nội dung
                    sheet.GetRow(row).CreateCell(col + 2);    // Ghi chú
                    sheet.GetRow(row).CreateCell(col + 3);    // Chi phí
                    sheet.GetRow(row).CreateCell(col + 4);    // Nhóm khoản mục

                    sheet.GetRow(row).GetCell(col).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 1).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 2).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 3).CellStyle = styleDark;
                    sheet.GetRow(row).GetCell(col + 4).CellStyle = styleDark;


                    sheet.GetRow(row).GetCell(col).SetCellValue(i.ToString());
                    sheet.GetRow(row).GetCell(col + 1).SetCellValue(item.Title);
                    sheet.GetRow(row).GetCell(col + 2).SetCellValue(item.Note);
                    sheet.GetRow(row).GetCell(col + 3).SetCellValue(item.Currency);
                    sheet.GetRow(row).GetCell(col + 4).SetCellValue(item.ItemName);

                    row++;
                    i++;
                }

                row = 6;
                var sheet2 = workbook.GetSheetAt(1);

                i = 1;

                foreach (var item in nhanVien)
                {
                    sheet2.CreateRow(row);

                    sheet2.GetRow(row).CreateCell(col);        // STT
                    sheet2.GetRow(row).CreateCell(col + 1);    // Nội dung
                    sheet2.GetRow(row).CreateCell(col + 2);    // Ghi chú
                    sheet2.GetRow(row).CreateCell(col + 3);    // Chi phí
                    sheet2.GetRow(row).CreateCell(col + 4);    // Nhóm khoản mục

                    sheet2.GetRow(row).GetCell(col).CellStyle = styleDark;
                    sheet2.GetRow(row).GetCell(col + 1).CellStyle = styleDark;
                    sheet2.GetRow(row).GetCell(col + 2).CellStyle = styleDark;
                    sheet2.GetRow(row).GetCell(col + 3).CellStyle = styleDark;
                    sheet2.GetRow(row).GetCell(col + 4).CellStyle = styleDark;


                    sheet2.GetRow(row).GetCell(col).SetCellValue(i.ToString());
                    sheet2.GetRow(row).GetCell(col + 1).SetCellValue(item.Title);
                    sheet2.GetRow(row).GetCell(col + 2).SetCellValue(item.Note);
                    sheet2.GetRow(row).GetCell(col + 3).SetCellValue(item.Currency);
                    sheet2.GetRow(row).GetCell(col + 4).SetCellValue(item.GroupName);

                    row++;
                    i++;
                }
                workbook.Write(downloadStream);
                workbook.Close();
                System.IO.File.Delete(fFile);
                return File(downloadStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export_chi_phi" + "_.xlsx");
            }
            catch (Exception e)
            {
                workbook.Close();
                System.IO.File.Delete(fFile);
                return new OkObjectResult(new GenericResult(false, e.Message));
            }
        }
    }
}