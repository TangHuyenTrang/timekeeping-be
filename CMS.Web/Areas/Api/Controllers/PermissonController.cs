﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Service.Interfaces;
using CMS.Ultilities.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMS.Web.Areas.Api.Controllers
{
    public class PermissonController : BaseController
    {
        #region Constructor
        private IPermissonService _permissonService;

        public PermissonController(IPermissonService permissonService)
        {
            _permissonService = permissonService;
        }
        #endregion Constructor

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            try
            {
                var data = _permissonService.GetAll();
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }
    }
}