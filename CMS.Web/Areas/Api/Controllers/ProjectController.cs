﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CMS.Data.Entities;
using CMS.Ultilities.Dtos;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace CMS.Web.Areas.Api.Controllers
{
    public class ProjectController : BaseController
    {
        #region Constructor
        private IProjectService _projectSerice;

        public ProjectController(IProjectService projectSerice)
        {
            _projectSerice = projectSerice;
        }
        #endregion Constructor

        #region GET
        [HttpGet]
        [Route("GetAll")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetAll()
        {
            try
            {
                    var data = _projectSerice.GetAll();
                    return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetById")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetById(int id)
        {
            try
            {
                var data = _projectSerice.GetById(id);
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetAllByUser")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult GetAllByUser()
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity.Claims.Count() != 0)
                {
                    var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var data = _projectSerice.GetAllByUser(Guid.Parse(userid));
                    return new OkObjectResult(new GenericResult(true, data));
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "Bạn cần đăng nhập lại"));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }
        #endregion GET

        #region POST
        [HttpPost]
        [Route("Add")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Add([FromBody] ProjectCreateViewModel project)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity.Claims.Count() != 0)
                {
                    var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var data = _projectSerice.Add(project, Guid.Parse(userid));
                    return new OkObjectResult(new GenericResult(true, data));
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "Bạn cần đăng nhập lại"));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }
        #endregion POST

        #region PUT
        [HttpPut]
        [Route("Update")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Update([FromBody] ProjectViewModel project)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }

            else
            {
                try
                {
                    var identity = HttpContext.User.Identity as ClaimsIdentity;
                    if (identity != null)
                    {
                        var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                        var data = _projectSerice.Update(project, Guid.Parse(userid));
                        return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(false, "Bạn không đủ quyền"));
                    }

                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion POST

        #region DELETE
        [HttpDelete]
        [Route("Delete")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_production)]
        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new BadRequestObjectResult(new GenericResult(false, "Id is Requied"));
            }
            else
            {
                try
                {
                    var identity = HttpContext.User.Identity as ClaimsIdentity;
                    if (identity != null)
                    {
                        var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                       _projectSerice.Delete(id, Guid.Parse(userid));
                        return new OkObjectResult(new GenericResult(true, "DeleteSuccess!!!"));
                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(false, "Bạn không đủ quyền"));
                    }
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
    }
    #endregion DELETE
}