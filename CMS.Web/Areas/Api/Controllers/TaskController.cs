﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Data.Entities;
using CMS.Ultilities.Dtos;
using CMS.Web.Service.Interfaces;
using CMS.Web.Service.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace CMS.Web.Areas.Api.Controllers
{
    public class TaskController : BaseController
    {
        #region Constructor
        private ITaskService _taskService;

        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        #endregion Constructor

        #region GET
        [HttpGet]
        [Route("GetAll")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var data = await _taskService.GetAll();
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetByIdProject")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public async Task<IActionResult> GetByIdProject(int idProject)
        {
            try
            {
                var data = await _taskService.GetByIdProject(idProject);
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetById")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var data = await _taskService.GetById(id);
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetHistoryTask")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public async Task<IActionResult> GetHistoryTask(int taskId)
        {
            try
            {
                var data = await _taskService.GetHistoryTask(taskId);
                return new OkObjectResult(new GenericResult(true, data));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetNoti")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public async Task<IActionResult> GetNoti(DateTime dateNow)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity.Claims.Count() != 0)
                {
                    var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var data = _taskService.GetNoti(Guid.Parse(userid), dateNow);
                    return new OkObjectResult(new GenericResult(true, data));
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "Bạn cần đăng nhập lại"));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }


        [HttpGet]
        [Route("GetByUser")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public async Task<IActionResult> GetByUser(int projectId)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity.Claims.Count() != 0)
                {
                    var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var data = _taskService.GetByUser(Guid.Parse(userid), projectId);
                    return new OkObjectResult(new GenericResult(true, data));
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "Bạn cần đăng nhập lại"));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        [Route("GetProjectByUser")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public async Task<IActionResult> GetProjectByUser()
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity.Claims.Count() != 0)
                {
                    var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var data = _taskService.GetProjectByUser(Guid.Parse(userid));
                    return new OkObjectResult(new GenericResult(true, data));
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "Bạn cần đăng nhập lại"));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }
        #endregion GET

        #region POST
        [HttpPost]
        [Route("Add")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Add([FromBody] TaskCreateViewModel project)
        {
            if (!ModelState.IsValid)
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(new GenericResult(false, allErrors));
            }

            else
            {
                try
                {
                    _taskService.Add(project);

                    return new OkObjectResult(new GenericResult(true, "Add success!!!"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion POST

        #region PUT
        [HttpPut]
        [Route("Update")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_library)]
        public IActionResult Update([FromBody] TaskViewModel project)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity.Claims.Count() != 0)
                {
                    var userid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var data = _taskService.Update(project, Guid.Parse(userid));
                    return new OkObjectResult(new GenericResult(true, data));
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "Bạn cần đăng nhập lại"));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }
        #endregion POST

        #region DELETE
        [HttpDelete]
        [Route("Delete")]
        //[AppAuthorize(PermissionTypes.Any, PermissionRule.update_production)]
        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new BadRequestObjectResult(new GenericResult(false, "Id is Requied"));
            }
            else
            {
                try
                {
                    _taskService.Delete(id);

                    return new OkObjectResult(new GenericResult(true, "DeleteSuccess"));
                }
                catch (Exception ex)
                {
                    return new OkObjectResult(new GenericResult(false, ex.Message));
                }
            }
        }
        #endregion DELETE
    }
}
