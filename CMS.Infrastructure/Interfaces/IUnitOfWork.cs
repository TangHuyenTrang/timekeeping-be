﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Infrastructure.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Call save change from db context
        /// </summary>

        int SaveChanges();
    }
}
