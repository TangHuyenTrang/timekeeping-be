﻿using CMS.Data.Entities;
using CMS.Ultilities.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace CMS.Data.EF
{
    public class DbInitializer
    {
        private readonly AppDbContext _context;
        private UserManager<AppUser> _userManager;
        private RoleManager<AppRole> _roleManager;

        public DbInitializer(AppDbContext context, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Seed()
        {
            //if (!_roleManager.Roles.Any())
            //{
            //    await _roleManager.CreateAsync(new AppRole()
            //    {
            //        Name = "Admin",
            //        NormalizedName = "Admin",
            //        Description = "Top manager"
            //    });
            //    await _roleManager.CreateAsync(new AppRole()
            //    {
            //        Name = "Employee",
            //        NormalizedName = "Employee",
            //        Description = "Employee"
            //    });
            //}
            //if (!_userManager.Users.Any())
            //{
            //    await _userManager.CreateAsync(new AppUser()
            //    {
            //        UserName = "admin",
            //        FullName = "Administrator",
            //        Email = "admin@gmail.com",
            //        DateCreated = DateTime.Now,
            //        DateModified = DateTime.Now,
            //        Status = Status.Active
            //    }, "123456");
            //    var user = await _userManager.FindByNameAsync("admin");
            //    await _userManager.AddToRoleAsync(user, "Admin");
            //}
            
            ////save change
            //await _context.SaveChangesAsync();
        }
    }
}
