﻿using CMS.Data.Entities;
using CMS.Data.Interfaces;
using CMS.Infrastructure.SharedCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CMS.Data.EF
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }
        public virtual DbSet<AppUser> AppUser { get; set; }
        public DbSet<AppRole> AppRole { get; set; }
        public virtual DbSet<Permisson> Permisson { get; set; }
        public virtual DbSet<RolePermisson> RolePermisson { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<Cost> Cost { get; set; }
        public virtual DbSet<GroupCost> GroupCost { get; set; }
        public virtual DbSet<ItemsCost> ItemsCost { get; set; }
        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<ServiceProject> ServiceProject { get; set; }
        public virtual DbSet<Specialization> Specialization { get; set; }  
        public virtual DbSet<UserSpecialization> UserSpecialization { get; set; }
        public virtual DbSet<TimeKeeping> TimeKeeping { get; set; }
        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<PublicHoliday> PublicHoliday { get; set; }
        public virtual DbSet<HistoryTaskUser> HistoryTaskUser { get; set; }
        public virtual DbSet<EmailCode> EmailCode { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Identity Config

            builder.Entity<IdentityUserClaim<Guid>>().ToTable("AppUserClaims").HasKey(x => x.Id);

            builder.Entity<IdentityRoleClaim<Guid>>().ToTable("AppRoleClaims")
                .HasKey(x => x.Id);

            builder.Entity<IdentityUserLogin<Guid>>().ToTable("AppUserLogins").HasKey(x => x.UserId);

            builder.Entity<IdentityUserRole<Guid>>().ToTable("AppUserRoles")
                .HasKey(x => new { x.RoleId, x.UserId });
            builder.Entity<IdentityUserRole<Guid>>().HasKey(p => new { p.UserId, p.RoleId });

            builder.Entity<IdentityUserToken<Guid>>().ToTable("AppUserTokens")
               .HasKey(x => new { x.UserId });
            #endregion Identity Config
        }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
               .Where(x => x.Entity is IDomainEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                var entity = (IDomainEntity)entry.Entity;
                DateTime now = DateTime.Now;

                if (entry.State == EntityState.Added)
                {
                    entity.DateCreated = now;
                }
                else
                {
                    base.Entry(entity).Property(x => x.DateCreated).IsModified = false;
                }

                entity.DateModified = now;
            }
            return base.SaveChanges();
        }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json").Build();
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString);
            return new AppDbContext(builder.Options);
        }
    }
}
