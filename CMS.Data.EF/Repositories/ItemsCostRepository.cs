﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class ItemsCostRepository : EFRepository<ItemsCost>, IItemsCostRepository
    {
        private AppDbContext _appContext;
        public ItemsCostRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
