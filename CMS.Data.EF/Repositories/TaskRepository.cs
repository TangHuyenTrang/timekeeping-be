﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
namespace CMS.Data.EF.Repositories
{
    public class TaskRepository : EFRepository<Task>, ITaskRepository
    {
        private AppDbContext _appContext;
        public TaskRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
