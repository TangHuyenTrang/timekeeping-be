﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;

namespace CMS.Data.EF.Repositories
{
    public class ProjectRepository : EFRepository<Project>, IProjectRepository
    {
        private AppDbContext _appContext;
        public ProjectRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
