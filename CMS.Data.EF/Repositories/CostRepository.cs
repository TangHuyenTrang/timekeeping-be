﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;


namespace CMS.Data.EF.Repositories
{
    public class CostRepository : EFRepository<Cost>, ICostRepository
    {
        private AppDbContext _appContext;
        public CostRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
