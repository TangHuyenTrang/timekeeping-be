﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class GroupCostRepository : EFRepository<GroupCost>, IGroupCostRepository
    {
        private AppDbContext _appContext;
        public GroupCostRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
