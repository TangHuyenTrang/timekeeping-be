﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
   public  class RoleRepository : EFRepository<AppRole>, IRoleRepository
    {
        private AppDbContext _appContext;
        public RoleRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
