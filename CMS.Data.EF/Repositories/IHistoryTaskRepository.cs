﻿using CMS.Data.Entities;
using CMS.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public interface IHistoryTaskRepository : IRepository<HistoryTaskUser>
    {
    }
}
