﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class SpecializationRepository : EFRepository<Specialization>, ISpecializationRepository
    {
        private AppDbContext _appContext;
        public SpecializationRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
