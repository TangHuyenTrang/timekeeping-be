﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class UserRoleRepository : EFRepository<UserRole>, IUserRoleRepository
    {
        private AppDbContext _appContext;
        public UserRoleRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
