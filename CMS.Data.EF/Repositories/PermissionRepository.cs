﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class PermissionRepository : EFRepository<Permisson>, IPermissionRepository
    {
        private AppDbContext _appContext;
        public PermissionRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
