﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class UserRepository : EFRepository<AppUser>, IUserRepository
    {
        private AppDbContext _appContext;
        public UserRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
