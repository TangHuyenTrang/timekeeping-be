﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class TimeKeepingRepository : EFRepository<TimeKeeping>, ITimeKeepingRepository
    {
        private AppDbContext _appContext;
        public TimeKeepingRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
