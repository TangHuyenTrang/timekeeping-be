﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class EmailCodeRepository : EFRepository<EmailCode>, IEmailCodeRepository
    {
        private AppDbContext _appContext;
        public EmailCodeRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
