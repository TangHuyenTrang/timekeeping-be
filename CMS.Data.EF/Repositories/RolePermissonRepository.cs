﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class RolePermissonRepository : EFRepository<RolePermisson>, IRolePerRepository
    {
        private AppDbContext _appContext;
        public RolePermissonRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
