﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class PublicHolidayRepository : EFRepository<PublicHoliday>, IPublicHolidayRepository
    {
        private AppDbContext _appContext;
        public PublicHolidayRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
