﻿using CMS.Data.Entities;
using CMS.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class ServRepository : EFRepository<Service>, IServRepository
    {
        private AppDbContext _appContext;
        public ServRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
