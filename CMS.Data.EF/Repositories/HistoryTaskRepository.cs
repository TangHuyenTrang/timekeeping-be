﻿using CMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Data.EF.Repositories
{
    public class HistoryTaskRepository : EFRepository<HistoryTaskUser>, IHistoryTaskRepository
    {
        private AppDbContext _appContext;
        public HistoryTaskRepository(AppDbContext context) : base(context)
        {
            _appContext = context;
        }
    }
}
